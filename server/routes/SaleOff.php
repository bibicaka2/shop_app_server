<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('SaleOff/create', [
            'as' => 'saleOff.create',
            'uses' => 'SaleOffController@create',
        ]);

        $api->post('SaleOff/update', [
            'as' => 'saleOff.update',
            'uses' => 'SaleOffController@update',
        ]);

        $api->post('SaleOff/delete', [
            'as' => 'saleOff.delete',
            'uses' => 'SaleOffController@delete',
        ]);

        $api->post('SaleOff/list', [
            'as' => 'saleOff.list',
            'uses' => 'SaleOffController@list',
        ]);
         $api->post('SaleOff/find', [
            'as' => 'saleOff.find',
            'uses' => 'SaleOffController@find',
        ]);
         $api->post('SaleOff/check', [
            'as' => 'saleOff.check',
            'uses' => 'SaleOffController@check_code',
        ]);

    });
});
