<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('task/create', [
            'as' => 'task.create',
            'uses' => 'TaskController@create',
        ]);

        $api->post('task/update', [
            'as' => 'task.update',
            'uses' => 'TaskController@update',
        ]);

        $api->post('task/delete', [
            'as' => 'task.delete',
            'uses' => 'TaskController@delete',
        ]);

        $api->get('task/view', [
            'as' => 'task.view',
            'uses' => 'TaskController@view',
        ]);
         $api->get('task/todoid', [
            'as' => 'task.todoid',
            'uses' => 'TaskController@find',
        ]);
     
     
    });
});
