<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('todos/create', [
            'as' => 'todos.create',
            'uses' => 'TodosController@create',
        ]);

        $api->post('todos/update', [
            'as' => 'toDos.update',
            'uses' => 'TodosController@update',
        ]);

        $api->post('todos/delete', [
            'as' => 'toDos.delete',
            'uses' => 'TodosController@delete',
        ]);

        $api->get('todos/view', [
            'as' => 'toDos.view',
            'uses' => 'TodosController@view',
        ]);
         $api->post('todos/find', [
            'as' => 'toDos.find',
            'uses' => 'TodosController@find',
        ]);
         $api->post('todos/check', [
            'as' => 'toDos.check',
            'uses' => 'TodosController@check_code',
        ]);

    });
});
