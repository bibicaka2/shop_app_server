<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('billinfo/create', [
            'as' => 'billinfo.create',
            'uses' => 'BillInfoController@create',
        ]);
        $api->post('billinfo/update', [
            'as' => 'billinfo.update',
            'uses' => 'BillInfoController@update',
        ]);
        $api->post('billinfo/delete', [
            'as' => 'billinfo.delete',
            'uses' => 'BillInfoController@delete',
        ]);
        $api->post('billinfo/find', [
            'as' => 'billinfo.find',
            'uses' => 'BillInfoController@findBillById',
        ]);
        
    });
        


});
