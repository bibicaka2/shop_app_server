<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version APIs
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('Comments/create', [
            'as' => 'Comments.create',
            'uses' => 'CommentsController@create',
        ]);
         $api->post('Comments/update', [
            'as' => 'Comments.update',
            'uses' => 'CommentsController@update',
        ]);
          $api->post('Comments/delete', [
            'as' => 'Comments.delete',
            'uses' => 'CommentsController@delete',
        ]);
           $api->post('Comments/list', [
            'as' => 'Comments.list',
            'uses' => 'CommentsController@list',
        ]);
    });
        


});
