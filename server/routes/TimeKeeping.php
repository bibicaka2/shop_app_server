     <?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('TimeKeeping/register', [
            'as' => 'TimeKeeping.register',
            'uses' => 'TimeKeepingController@create',
        ]);
         $api->post('TimeKeeping/update', [
            'as' => 'TimeKeeping.update',
            'uses' => 'TimeKeepingController@update',
        ]);
          $api->post('TimeKeeping/delete', [
            'as' => 'TimeKeeping.delete',
            'uses' => 'TimeKeepingController@delete',
        ]);
           $api->post('TimeKeeping/view', [
            'as' => 'TimeKeeping.view',
            'uses' => 'TimeKeepingController@view_shift_work',
        ]);
            $api->post('TimeKeeping/view1', [
            'as' => 'TimeKeeping.view1',
            'uses' => 'TimeKeepingController@view',
        ]);

            $api->post('TimeKeeping/check', [
            'as' => 'TimeKeeping.check',
            'uses' => 'TimeKeepingController@check',
        ]);
            $api->post('TimeKeeping/view2', [
            'as' => 'TimeKeeping.view2',
            'uses' => 'TimeKeepingController@view_shift_work_1',
        ]);
            $api->post('TimeKeeping/test2', [
            'as' => 'TimeKeeping.test2',
            'uses' => 'TimeKeepingController@Test_2',
        ]);
            $api->post('TimeKeeping/view3', [
            'as' => 'TimeKeeping.Test2',
            'uses' => 'TimeKeepingController@view_shift_work_2',
        ]);
             $api->post('TimeKeeping/test3', [
            'as' => 'TimeKeeping.Test3',
            'uses' => 'TimeKeepingController@test_3',
        ]);
               $api->post('TimeKeeping/test4', [
            'as' => 'TimeKeeping.Test4',
            'uses' => 'TimeKeepingController@test_4',
        ]);
            
    });
        


});
