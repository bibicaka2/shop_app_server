<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('branch/register', [
            'as' => 'branch.register',
            'uses' => 'BranchController@create',
        ]);
         $api->post('Branch/update', [
            'as' => 'Branch.update',
            'uses' => 'BranchController@update',
        ]);
          $api->post('Branch/delete', [
            'as' => 'Branch.delete',
            'uses' => 'BranchController@delete',
        ]);
           $api->get('branch/view', [
            'as' => 'branch.view',
            'uses' => 'BranchController@view',
        ]);
           $api->post('Branch/Test', [
            'as' => 'Branch.Test',
            'uses' => 'BranchController@Test',
        ]);
            $api->post('Branch/Test1', [
            'as' => 'Branch.Test1',
            'uses' => 'BranchController@Test_1',
        ]);
             $api->post('Branch/Test2', [
            'as' => 'Branch.Test2',
            'uses' => 'BranchController@Test_2'
        ]);
            $api->post('Branch/Test3', [
            'as' => 'Branch.Test3',
            'uses' => 'BranchController@Test_3',
        ]);
                          
    });
        


});
