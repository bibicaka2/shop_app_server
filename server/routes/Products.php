<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('Products/create', [
            'as' => 'products.create',
            'uses' => 'ProductsController@create',
        ]);

        $api->post('Products/update', [
            'as' => 'products.update',
            'uses' => 'ProductsController@update',
        ]);

        $api->post('Products/delete', [
            'as' => 'products.delete',
            'uses' => 'ProductsController@delete',
        ]);
        $api->post('Products/get-type-product', [
            'as' => 'products.get-type-product',
            'uses' => 'ProductsController@GetProductByType',
        ]);

        $api->get('Products/list', [
            'as' => 'products.list',
            'uses' => 'ProductsController@list',
        ]);
         $api->post('Products/findProduct', [
            'as' => 'products.findProduct',
            'uses' => 'ProductsController@findProduct',
        ]);

    });
});
