<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('ProductStatus/create', [
            'as' => 'productStatus.create',
            'uses' => 'ProductStatusController@create',
        ]);

        $api->post('ProductStatus/update', [
            'as' => 'productStatus.update',
            'uses' => 'ProductStatusController@update',
        ]);

        $api->post('ProductStatus/delete', [
            'as' => 'productStatus.delete',
            'uses' => 'ProductStatusController@delete',
        ]);

        $api->post('ProductStatus/list', [
            'as' => 'productStatus.list',
            'uses' => 'ProductStatusController@list',
        ]);
         $api->post('ProductStatus/findProduct', [
            'as' => 'productStatus.findProduct',
            'uses' => 'ProductStatusController@findProduct',
        ]);

    });
});
