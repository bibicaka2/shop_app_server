     <?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('Login/register', [
            'as' => 'Login.register',
            'uses' => 'LoginController@create',
        ]);
         $api->post('Login/update', [
            'as' => 'Login.update',
            'uses' => 'LoginController@update',
        ]);
          $api->post('Login/delete', [
            'as' => 'Login.delete',
            'uses' => 'LoginController@delete',
        ]);
           $api->post('Login/view', [
            'as' => 'Login.view',
            'uses' => 'LoginController@view',
        ]);
           $api->post('Login/loginn', [
            'as' => 'Login.loginn',
            'uses' => 'LoginController@login_1',
        ]);
    });
        


});
