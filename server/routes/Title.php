<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('Title/register', [
            'as' => 'Title.register',
            'uses' => 'TitleController@create',
        ]);
         $api->post('Title/update', [
            'as' => 'Title.update',
            'uses' => 'TitleController@update',
        ]);
          $api->post('Title/delete', [
            'as' => 'Title.delete',
            'uses' => 'TitleController@delete',
        ]);
           $api->post('Title/view', [
            'as' => 'Title.view',
            'uses' => 'TitleController@view',
        ]);
    });
        


});
