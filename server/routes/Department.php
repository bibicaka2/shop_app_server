<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('Department/register', [
            'as' => 'Department.register',
            'uses' => 'DepartmentController@create',
        ]);
         $api->post('Department/update', [
            'as' => 'Department.update',
            'uses' => 'DepartmentController@update',
        ]);
          $api->post('Department/delete', [
            'as' => 'Department.delete',
            'uses' => 'DepartmentController@delete',
        ]);
           $api->post('Department/view', [
            'as' => 'Department.view',
            'uses' => 'DepartmentController@view',
        ]);
    });
        


});
