<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('ProductType/create', [
            'as' => 'productType.create',
            'uses' => 'ProductTypeController@create',
        ]);

        $api->post('ProductType/update', [
            'as' => 'productType.update',
            'uses' => 'ProductTypeController@update',
        ]);

        $api->post('ProductType/delete', [
            'as' => 'productType.delete',
            'uses' => 'ProductTypeController@delete',
        ]);

        $api->post('ProductType/list', [
            'as' => 'productType.list',
            'uses' => 'ProductTypeController@list',
        ]);
         $api->post('ProductType/findProduct', [
            'as' => 'productType.findProduct',
            'uses' => 'ProductTypeController@findProduct',
        ]);

    });
});
