<?php

namespace app\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $models = array(
            'Users',
            'UsersInfo',
            'UserToken',
            'Student',
            'Branch',
            'Staff',
            'Title',
            'TimeKeeping',
            'ShiftWork',
            'Company',
            'Department',
            'SabbaticalLeave',
            'Login',
            'Test',
            'ChangLan',
            'Products',
            'BillInfo',
            'Comments',
            'ProductSize',
            'ProductStatus',
            'SaleOff',
            'Todos',
            'Task',
            'ProductType',
            'VerificationUser'
        );

        foreach ($models as $model) {
            $this->app->bind("App\\Api\\Repositories\\Contracts\\{$model}Repository", "App\\Api\\Repositories\\Eloquent\\{$model}RepositoryEloquent");
        }
    }
}
