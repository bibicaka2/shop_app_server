<?php
namespace App\Libraries\Gma\APIs;
use Illuminate\Http\Request;
use Redirect,Response,DB,Config;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mails\MailNotify;
class APISendMail {
    /**
     * Gửi mail thông báo thông tin tài khoản
     * Param truyền vào cần có:
     * user_name: tên đăng nhập
     * user_employee_name: họ và tên nhân viên
     * password: mật khẩu đăng nhập
     * branch_name: tên chi nhánh làm việc
     * org_name: tên tổ chức/doanh nghiệp
     * date: ngày tháng năm theo định dạng dd-MM-YYYY
     * account_type: loại tài khoản (dùng thử/có bản quyền)
     * position: vị trí làm việc (Manager/Branch's Manager/Staff)
     * position_detail: chi tiết vị trí công việc (ví dụ: Staff = [full time, part time])
     */
    public static function mailForNotificateAccountInfo($mailParams = []){

    }
    public static function sendMail($user,$number_verification=0000){           
           $data = array('name'=>"YTC" ,"verification_code"=>$number_verification);
        Mail::to($user)->send(new MailNotify($data));
    }
}