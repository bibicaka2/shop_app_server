<?php

namespace App\Libraries\Gma\APIs;

use App\Api\Entities\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Illuminate\Support\Str;

class APIAuth
{
    /**
     * API tạo tài khoản người dùng
     */
    public static function createUserAccount($params = [])
    {
        $user_id = $params['user_id'];
        $password = password_hash($params['password'], PASSWORD_BCRYPT);
     //   $user_name = $params['user_name'];
        $phone = $params['phone'];
        $email = $params['email'];
      //    $address = $params['address'];
      //  $no_sign = $params['no_sign'];
        //$noSignProfile = $params['no_sign_profile'];

        $attribute = [
            'user_id' => $user_id,
           // 'user_name' => $user_name,
            'password' => $password,
            'phone' => $phone,
            'email' => $email,
            // 'no_sign' => $no_sign,
            // 'no_sign_profile' => $noSignProfile
        ];

        $userAccount = Users::create($attribute);
        return trans('core.success');
    }

    // /**
    //  * Xoá tài khoản người dùng
    //  */
    // public static function deleteAccount($user_id = ''){
    //     if(!empty($user_id)){
    //         $account_info = Users::where([
    //             'user_id'=>mongo_id($user_id)
    //         ])->first();

    //         if(!empty($account_info)){
    //             $account_info->deleted_user = Auth::getPayLoad('user_id');
    //             $account_info->save();
    //             $account_info->delete();

    //             return trans('core.success');
    //         }
    //     }
    //     return trans('employee.no_employee');
    // }
    public static function smsTwilio(string $verify_code = '')
    {
        //tạo mã xác thực
        // $otp_token = mt_rand(10000, 99999);

        //các số điện thoại sẽ nhận tin nhắn
        //do là bản xài thử nên chỉ có thể tự gửi sms cho chính mình
        $client = new Client(env('ACCOUNT_SID'), env('AUTH_TOKEN'));
        $client->messages->create(
            '+84563243215',
            array(
                'from' => env('TWILIO_NUMBER'),
                'body' => 'Phone verification code: ' . $verify_code
            )
        );
    }
}
