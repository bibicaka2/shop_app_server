<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\ProductTypeRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;
use App\Api\Entities\ProductType;


class ProductTypeController extends Controller
{
    protected $productTypeRepository;
    protected $request;

    public function __construct(   
        ProductTypeRepository $productTypeRepository, 
        Request $request
    )
        {
            $this->productTypeRepository = $productTypeRepository;
            $this->request = $request;
            parent::__construct();
        }
    
    public function create()
    {

        $validator = \validator::make($this->request->all(),[
            'name'=>'required',
            'img'=>'required',
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        
        $name = $this->request->get('name');
       
       $img = $this->request->get('img');

        $attributes = [        
            'name' => $this->request->get('name'),       
            'img' => $this->request->get('img'),      
        ];
        $productType =$this->productTypeRepository->create($attributes);
        return $this->successRequest($productType);
    }
    
    public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
          
        ]);
        $id = $this->request->get('_id');
       $Type =  $this->request->get('Type');
        $check = false;
        if($Type ==='true' )
        {
            $check = true;
        }
        $productType = productType::where('_id', mongo_id($id))->first();
        if (!empty($productType)) {    
            if(!empty($this->request->get('name')))
            {
                $productType->name=$this->request->get('name');
            }
         
            
            $productType->save();
        }
        return $this->successRequest($productType); 
    }
    
    public function delete()
    {

        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $productType=ProductType::where('_id',mongo_id($id))->first();
        if(!empty($productType))
        {   
            $productType->forceDelete();
        }
        return $this->successRequest();
    }
    public function list()
    {

       $productType = ProductType::all();
       if(count($productType)>0)
       {
         foreach ($productType as $key => $value) {
             $product_list[]= $value;
                }
                return $product_list;
       }
       return [];
    }
    public function findProduct()
    {
        $productType=[];
        $validator = \validator::make($this->request->all(),[
            'key'=>'required',
        ]);
        $key=$this->request->get('key');
        $key=build_slug($key);

        $params=[
            'key'=> $this->request->get('key')
        ];
        $productType=$this->productTypeRepository->getBranch($params);
        return $productType;



        $validator = \validator::make($this->request->all(),[
            'id'=>'required', 
           
        ]);
        $id = $this->request->get('_id');
       $ProductType = ProductType::where('_id',mongo_id($id))->first();
       
       return $ProductType;
    }
    public function listProductByType(){
        $ProductType=[];
        $validator =   $validator = \validator::make($this->request->all(),[
            'id'=>'required',   
        ]);
    }
}