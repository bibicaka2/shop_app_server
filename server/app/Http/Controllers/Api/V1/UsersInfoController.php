<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Status;
use App\Api\Entities\UserFriendRequest;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Api\Entities\VerificationUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Api\Repositories\Contracts\UsersInfoRepository;
use App\Api\Repositories\Contracts\UsersRepository;
use App\Api\Repositories\Contracts\VerificationUserRepository;
use App\Libraries\Gma\APIs\APIAuth;
use App\Libraries\Gma\APIs\APIUpload;
use App\Libraries\Gma\APIs\APISendMail;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UsersInfoController extends Controller
{
    protected $request;
    protected $userInfoRepository;
    protected $usersRepository;
    protected $verificationCode;
    public function __construct(Request $request, UsersInfoRepository $userInfoRepository, UsersRepository $usersRepository, VerificationUserRepository $verificationUserRepository)
    {
        $this->request = $request;
        $this->userInfoRepository = $userInfoRepository;
        $this->usersRepository = $usersRepository;
           $this->verificationUserRepository= $verificationUserRepository;
    }


    /**
     * api đăng kí người dùng
     * route: [POST]api/user/register
     * request url: http://tlcn-server-test/api/user/register
     * params:
     *      first_name: required
     *      last_name: required,
     *      user_name: required,
     *      password: required
     *      email: required
     *      phone: required|unique:users_info
     *      sex: [
                'required',
                Rule::in([0,1])]
            nation: required
            avatar: nullable|file
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công."
            }
     */
    public function register()
    {
        $validator = \Validator::make($this->request->all(), [
            'fullname'=>'required',
            // 'first_name' => 'required',
            // 'last_name' => 'required',
            'password' => 'required',
            'email' => 'required|email|unique:users_info',
            'phone' => 'required|unique:users_info|max:11|min:10',
             'address' => 'required',
            // 'sex' => [
            //     'required',
            //     Rule::in([0, 1])
            // ],
            // 'dOb' => 'required|date_format:d/m/Y',
            // 'avatar' => 'nullable|file'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        // $first_name = $this->request->get('first_name');
        // $last_name = $this->request->get('last_name');
        $fullname = $this->request->get('fullname');
      //  $user_name = $last_name . ' ' . $first_name;
  //      $dOb = $this->request->get('dOb');
        $password = $this->request->get('password');
        $email = $this->request->get('email');
        $phone = $this->request->get('phone');
        // $sex = $this->request->get('sex');
        // $avatar = $this->request->get('avatar');
        $address = $this->request->get('address');
        $isNumberString = is_number_string($phone);
        if ($isNumberString == -1) {
            return $this->errorBadRequest(trans('core.invalid_phone'));
        }

        //Lưu các thông tin cơ bản của người dùng vào collection userInfo
        $userInfoAttr = [
            // 'first_name' => $first_name,
            // 'last_name' => $last_name,
            'fullname'=>$fullname,
            'email' => $email,
            'phone' => $phone,
             'address' => $address,
            // 'sex' => $sex,
            // 'dOb' => $dOb,
            'phone_verified' => '0' //chưa xác thực,
        ];
        $userInfo = UsersInfo::create($userInfoAttr);

        // //build no sign profile
        // $createDateTime = DateTime::createFromFormat("d/m/Y", $dOb);
        // // $date = $createDateTime->format('d');
        // $noSignProfile = build_user_name(trim($last_name, '') . '.' . trim($first_name, ' ')) . '.' . $createDateTime->format('d') . '.' . $createDateTime->format('m');
        // //check no_sign
        // $noSignExisted = Users::where([
        //     'no_sign_profile' => $noSignProfile
        // ])->first();
        // if (!empty($noSignExisted)) {
        //     $randomStr = str_random(4);
        //     $noSignProfile = $noSignProfile . '.' . $randomStr;
        // }

        //Lưu các thông tin khác vào users
        $userAccountAttr = [
         //  'user_name' => $user_name,
          //  'no_sign' => build_slug($user_name),
          //  'no_sign_profile' => $noSignProfile,
            'user_id' => $userInfo->_id,
           // 'fullname'=>$fullname,
            'phone' => $phone,
          //   'address' => $address,
            'email' => $email,
            'password' => $password
        ];
        $userAccount = APIAuth::createUserAccount($userAccountAttr);

        //gửi sms để xác thực số điện thoại
        //tạo OTP random
        $verify_code = mt_rand(10000, 99999);
        $userInfo->verify_code = (string)$verify_code;
        $userInfo->save();

        // APIAuth::smsTwilio($verify_code);
        return $this->successRequest(trans('core.success'));
    }
    /**
     * api đăng kí người dùng
     * route: [POST]api/update/info
     * request url: http://tlcn-server-test/api/user/update/info
     * params:
     *      user_id: required
     *      first_name: nullable
     *      last_name: nullable
     *      user_name: nullable
     *      password: nullable
     *      email: nullable
     *      phone: nullable|unique:users_info
            nation: nullable
            avatar: nullable|file
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công."
            }
     */
    public function updateUserInfo()
    {
        $validator = \Validator::make($this->request->all(), [
            // 'user_id'=>'required',
            // 'email'=>'email|unique:users_info',
            // 'phone'=>'unique:users_info|max:11|min:10',
            'update_type' => [
                'required',
                Rule::in([0, 1, 2, 3, 4, 5])
                /**
                 * 0: cập nhật thông tin cá nhân
                 * 1: cập nhoật thông tin đăng nhập và bảo mật (tên hiển thị, mật khẩu đăng nhập)
                 * 2: cập nhật thông tin cài đặt web site theo từng cá nhân
                 * 3: cập nhật thông tin danh sách bạn bè
                 * 4: gửi OTP
                 * 5: xác thực OTP
                 */
            ],
            'file' => 'file',
            'password' => 'required_if:' . 'update_type,0'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $update_type = (int)($this->request->get('update_type'));
        //check user info and user login info
        if ($this->request->get('update_type') != 5) {
            $userAccount = Users::where([
                'user_id' => Auth::getPayLoad()->get('user_id')
            ])->first();
            if (empty($userAccount)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }

            $userInfo = UsersInfo::where([
                '_id' => mongo_id(Auth::getPayLoad()->get('user_id'))
            ])->first();
            if (empty($userInfo)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }
        }

        //Kiểm tra thông tin mật khẩu trước khi tiến hành cập nhật
        $password = $this->request->get('password');
        if (isset($password)) {
            if (!password_verify($password, $userAccount->password)) {
                return $this->errorBadRequest(trans('auth.incorrect_password_confirm'));
            }
        }

        //0: Cập nhật thông tin cá nhân cở bản (email, first_name, last_name, nation, avatar, cover_picture)
        if ($update_type == 0) {
            $validator = \Validator::make($this->request->all(), [
                'email' => 'email|unique:users_info',
                'phone' => 'unique:users_info|max:11|min:10',
                'dOb' => 'date_format:d/m/Y'
            ]);
            if ($validator->fails()) {
                return $this->errorBadRequest($validator->messages()->toArray());
            }
            $email = $this->request->get('email');
            if (isset($email)) {
                $userInfo->email = $email;
                $userAccount->email = $email;
            }

            $first_name = $this->request->get('first_name');
            if (isset($first_name) && !empty($first_name)) {
                $userInfo->first_name = $first_name;
                $userAccount->user_name = $userInfo->last_name . ' ' . $first_name;
                $userAccount->no_sign = build_slug($userInfo->last_name . ' ' . $first_name);
            }
            $last_name = $this->request->get('last_name');
            if (isset($last_name) && !empty($last_name)) {
                $userInfo->last_name = $last_name;
                $userAccount->user_name = $last_name . ' ' . $userInfo->first_name;
                $userAccount->no_sign = build_slug($last_name . ' ' . $userInfo->first_name);
            }

            $dOb = $this->request->get('dOb');
            if (isset($dOb)) {
                $userInfo->dOb = $dOb;
            }
            $phone = $this->request->get('phone');
            if (isset($phone)) {
                $isNumberString = is_number_string($phone);
                if ($isNumberString == -1) {
                    return $this->errorBadRequest(trans('core.invalid_phone'));
                }
                if (isset($userInfo->phone_verified)) {
                    $userInfo->phone = $phone;
                    $userAccount->phone = $phone;
                }
            }
        } //1: cập nhật các thông tin bảo mật (password)
        elseif ($update_type == 1) {
            $validator = \Validator::make($this->request->all(), [
                'old_password' => 'required',
                'new_password' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->errorBadRequest($validator->messages()->toArray());
            }

            $oldPassword = $this->request->get('old_password');
            $newPassword = $this->request->get('new_password');

            //kiểm tra xem password cũ
            if (password_verify($oldPassword, $userAccount->password)) {
                $errorData = [];

                if ((strlen($newPassword) < 8) || (strlen($newPassword) > 32)) {
                    if (strlen($newPassword) != 1) {
                        array_push($errorData, trans('auth.password_min_8_max_32'));
                    }
                }
                if ($newPassword == $oldPassword) {
                    array_push($errorData, trans('auth.same_old_password'));
                }

                if (!empty($errorData)) {
                    return $this->errorBadRequest($errorData);
                }
                $userAccount->password = password_hash($newPassword, PASSWORD_BCRYPT);
            } else {
                return $this->errorBadRequest(trans('auth.incorrect_old_password'));
            }
        } //2: cập nhật avatar & cover
        elseif ($update_type == 2) {
            $file = $this->request->file('file');
            if (!empty($file)) {
                $validator = \Validator::make($this->request->all(), [
                    'type' => [
                        'required',
                        Rule::in([0, 1]) //0: image, 1: file
                    ],
                    'option' => [
                        'required',
                        Rule::in([0, 1, 2]) //0: avatars, 1: cover_picture, 2: others
                    ]
                ]);

                if ($validator->fails()) {
                    return $this->errorBadRequest($validator->messages()->toArray());
                }

                $user_id = Auth::getPayLoad()->get('user_id');
                $userInfo = UsersInfo::where([
                    '_id' => mongo_id($user_id)
                ])->first();

                if (empty($userInfo)) {
                    return $this->errorBadRequest(trans('user.account_not_exist'));
                }

                $param = [
                    'user_id' => $userInfo->_id,
                    'type' => $this->request->get('type'),
                    'option' => $this->request->get('option')
                ];
                $type = (int)($this->request->get('type'));
                $option = (int)($this->request->get('option'));

                if ($type == 0) {
                    $param['type'] = 'image';
                } else {
                    $param['type'] = 'files';
                }
                if ($option == 0) {
                    $param['option'] = 'avatars';
                } elseif ($option == 1) {
                    $param['option'] = 'cover_picture';
                } else {
                    $param['option'] = 'others';
                }

                //upload with avatar || cover option
                $statusImageLink = APIUpload::uploadToServer($param, $file);
                $linkImage = APIUpload::getFileToClient($param);

                //đăng trạng thái sau khi update ảnh bìa hoặc ảnh đại diện
                $param['option'] = 'status';
                $imgArray = [];
                array_push($imgArray, $statusImageLink);
                $statusParams = [
                    'status_setting' => 'pub',
                    'user_id' => Auth::getPayLoad()->get('user_id'),
                    'upload_ids' => $imgArray,
                    'header_content' => $option
                ];
                Status::create($statusParams);

                return $this->successRequest($linkImage);
            }
            return $this->errorBadRequest(trans('core.pls_input_image'));
        } //3: cập nhật thông tin bạn bè
        elseif ($update_type == 3) {
            $validator = \Validator::make($this->request->all(), [
                'friend_id' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->errorBadRequest($validator->messages()->toArray());
            }

            $friend_id = $this->request->get('friend_id');
            //check friend
            $friendAccount = Users::where([
                'user_id' => $friend_id
            ])->first();
            if (empty($friendAccount)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }
            $friendInfo = UsersInfo::where([
                '_id' => mongo_id($friend_id)
            ])->first();
            if (empty($friendInfo)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }

            if (isset($userInfo->list_friends)) {
                $list_friends = (array)$userInfo->list_friends;
                array_push($list_friends, $friend_id);

                $listFriendSender = (array)$friendInfo->list_friends;
                array_push($listFriendSender, $userInfo->_id);

                $userInfo->list_friends = $list_friends;
                $friendInfo->list_friends = $listFriendSender;
            } else {
                $userInfoNewListFriend = [];
                $friendInfoNewListFriend = [];

                array_push($userInfoNewListFriend, $friend_id);
                array_push($friendInfoNewListFriend, $userInfo->_id);

                $userInfo->list_friends = $userInfoNewListFriend;
                $friendInfo->list_friends = $friendInfoNewListFriend;
            }
            $friendInfo->save();
        } //4: gửi OTP
        elseif ($update_type == 4) {
            //tạo OTP random
            $verify_code = mt_rand(10000, 99999);
            $userInfo->verify_code = (string)$verify_code;
            $userInfo->unset('phone_verified');
            APIAuth::smsTwilio($verify_code);
        } //5: xác thực OTP
        else {
            $validator = \Validator::make($this->request->all(), [
                'otp_token' => 'required',
                'phone' => 'max:11|min:10'
            ]);

            if ($validator->fails()) {
                return $this->errorBadRequest($validator->messages()->toArray());
            }

            $phone = $this->request->get('phone');
            if (isset($phone)) {
                $userInfo = UsersInfo::where([
                    'phone' => $phone
                ])->first();
                if (empty($userInfo)) {
                    return $this->errorBadRequest(trans('user.account_not_exist'));
                }

                $isNumberString = is_number_string($phone);
                if (!$isNumberString) {
                    return $this->errorBadRequest(trans('core.invalid_phone'));
                }
            }

            //kiểm tra OTP có chính xác hay không nếu chính xác thì phone_verified = 1
            $otp_token = $this->request->get('otp_token');
            if ($userInfo->verify_code != $otp_token) {
                return $this->errorBadRequest(trans('auth.invalid_otp'));
            }
            $userInfo->phone_verified = 1;
        }

        if (!empty($userInfo)) {
            $userInfo->save();
        }
        if (!empty($userAccount)) {
            $userAccount->save();
        }

        return $this->successRequest(trans('core.success'));
    }

    public function updateUserInfoApp(){
              $validator = \Validator::make($this->request->all(), [
          
             'type' => [
                        'required',
                        Rule::in([0, 1])
                    ],
        ]);
    
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $type = (int)($this->request->get('type'));
        $user_id = Auth::getPayLoad()->get('user_id');
        $userInfo = UsersInfo::where('_id',mongo_id($user_id))->first();
        if(empty($userInfo)){
            return $this->errorBadRequest(trans('user info is valid'));
        }
        $user = Users::where('user_id',$user_id)->first();
        if(empty($user)){
             return $this->errorBadRequest(trans('user is valid'));
        }
        if($type=0){
            $validator = \Validator::make($this->request->all(), [
            'fullname'=>'required',
            'address' => 'required',
            ]);
            $fullname = $this->request->get('fullname');
            $address = $this->request->get('address');
            if(!empty($fullname)){
                $userInfo->fullname = $fullname;
            }
            if(!empty($address)){
            $userInfo->address = $address;
            }
            $userInfo->save();
        }
        if($type=1){
            $validator = \Validator::make($this->request->all(), [
            'new_password' => 'required',
            'old_password' => 'required',
            ]);
            $password = $this->request->get('new_password');
            $password_old = $this->request->get('old_password');

            if(!empty($password)&&password_verify($password_old, $user->password)){
                $user->password = password_hash($password, PASSWORD_BCRYPT);
                $user->save();
            }
            else{
                return $this->errorBadRequest(trans('Change Password is error'));
            }

        }
     
        return $this->successRequest('SUCCESS');
    }
    /**
     * api xoá tài khoản người dùng
     * route: [POST]api/user/delete
     * request url: http://tlcn-server-test/api/user/delete
     * params:
     *      user_id: required
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công."
            }
     */
    public function deleteUserAccount()
    {
        $validator = \Validator::make($this->request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $user_id = Auth::getPayLoad()->get('user_id');
        //check userInfo
        $userAccount = Users::where([
            'user_id' => $user_id
        ])->first();
        if (empty($userAccount)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }
        $userInfo = UsersInfo::where([
            '_id' => mongo_id($user_id)
        ])->first();
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        //xoá id ở danh sách bạn bè
        $user = $this->userInfoRepository->getUserInfo();
        foreach ($user as $item) {
            $list_friends = $item->list_friends;
            if (isset($list_friends)) {
                for ($i = 0; $i < count($list_friends); $i++) {
                    if ($list_friends[$i] == $user_id) {
                        $list_friends[$i] = 'Undefined';
                    }
                }
                $item->list_friends = $list_friends;
                $item->save();
            }
        }

        //delete user account
        // $userAccount->deleted_user = Auth::getPayLoad()->get('user_id');
        // $userAccount->save();
        $userAccount->delete();

        //delete user info
        // $userInfo->deleted_user = Auth::getPayLoad()->get('user_id');
        // $userInfo->save();
        $userInfo->delete();
        return $this->successRequest(trans('core.success'));
    }
    /**
     * api tìm kiếm và lấy profile của user
     * route: [GET]api/user/search-v1
     * request url: http://tlcn-server-test/api/user/search-v1
     * params:
     *      type_search: required, Rule::in([0,1])
     *          0: tìm kiếm
     *          1: tìm kiếm xong bấm vào để hiện profile
     *      search_content: string nullable
     *      user_id: required nếu type search = 1
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": [
                    {
                        "user_id": "5f5c5c17ba3700002b00501c",
                        "user_name": "Cơ Khánh",
                        "first_name": "Khánh",
                        "last_name": "Dương Cơ",
                        "phone": "0563283215",
                        "email": "duongcokhanh@gmail.com",
                        "nation": "Việt Nam",
                        "sex": "Nam"
                    }
                ]
            }
     */
    public function userListV1()
    {
        $validator = \Validator::make($this->request->all(), [
            'type_search' => [
                'required',
                Rule::in([0, 1])
                /**
                 * 0: trả về danh sách các user có tên chứa kí tự cần tìm
                 * 1: trả về 1 user duy nhất (dùng để xem profile)
                 */
            ]
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $type_search = $this->request->get('type_search');
        $data = [];

        if ($type_search == 0) {
            $search_content = $this->request->get('search_content');
            $params = [
                'phone' => $search_content
            ];
            $user = $this->userInfoRepository->getUserInfo($params);
            //nếu tìm theo số điện thoại không có
            if (count($user) == 0) {
                $params = [
                    'user_name' => build_slug($search_content)
                ];
                $user = $this->usersRepository->getUsersAccount($params);
            }

            foreach ($user as $item) {
                $data[] = $item->transform();
            }
        } else {
            $validator = \Validator::make($this->request->all(), [
                'user_id' => 'required_without:' . 'token',
                'token' => 'required_without:' . 'user_id'
            ]);

            if ($validator->fails()) {
                return $this->errorBadRequest($validator->messages()->toArray());
            }

            $userIdNormal = $this->request->get('user_id');
            $user_id = '';
            if (isset($userIdNormal)) {
                $user_id = $userIdNormal;
            } else {
                $user_id = Auth::getPayLoad()->get('user_id');
            }

            $params = [
                '_id' => $user_id,
                'is_detail' => 1
            ];
            $user = $this->userInfoRepository->getUserInfo($params);

            if (empty($user)) {
                return $this->errorBadRequest(trans('users.account_not_exist'));
            }
            $data = $user->transform();
        }

        return $this->successRequest($data);
    }
    /**
     * api kiểm tra quan hệ giữa hai người dùng
     * route: [GET]api/user/check-relationship
     * request url: http://13.67.77.166/api/user/check-relationship
     * params:
     *      friend_id: required
     * headers:
     *      Authorization: bearer + token
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Bạn bè"
            }
     */
    public function forgetPass(){
          $validator = \Validator::make($this->request->all(), [
          'user_name'=>'required'
        ]);
        $user = $this->request->get('user_name');

        //check user name có tồn tại
        $params = [
            'phone' => $user,
        ];
        $user_list = $this->usersRepository->getUsersAccount($params);

        if (count($user_list) == 0) {
            $params = [
                'email' => $user,
            ];

            $user_list = $this->usersRepository->getUsersAccount($params);
        }

        if (count($user_list) == 0) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        foreach ($user_list as $item) {
            $user_record = $item;
        }
        $verification_code_old = VerificationUser::where('user_id',$user_record->user_id)->first();
        if(!empty($verification_code_old))
        {
            $verification_code_old->delete();
        }
        //djson($user_record);
       // djson($user_list[0]['user_name']);
        $verification_code=mt_rand(1000,9000); 
        $email= $user_record->email;
        $sendMail =APISendMail::sendMail($email,$verification_code);

        $attributes = [        
            'user_id' =>$user_record->user_id,   
            'verification_code'=> $verification_code,
        ];
       $verification_user =$this->verificationUserRepository->create($attributes);
        return $this->successRequest($attributes);

    }
    public  function resetPass(){
       $validator = \Validator::make($this->request->all(), [
            'user_id' => 'required',
            'code'=>'required'
        ]);

        $verification_code = $this->request->get('code');
        $user_id = $this->request->get('user_id');
        $user = Users::where('user_id',$user_id)->first();
         $verification_user = VerificationUser::where('user_id',$user_id)->first();
        if(empty($user) ||empty($verification_user) ){
              return $this->errorBadRequest(trans('$user is valid'));
        }
       if($verification_user->verification_code == $verification_code)
       {
            $password="000000";
            $password_hash = password_hash(  $password, PASSWORD_BCRYPT);
            //$b= password_needs_rehash($password,PASSWORD_BCRYPT);
            // djson($b);
            $user->password= $password_hash;
            $verification_user->delete();
            $user->save();
            return $this->successRequest(trans('Resset success'));
        }
        return $this->successRequest(trans('Resset fail!!'));
    }

    public function checkUserRelationship()
    {
        $validator = \Validator::make($this->request->all(), [
            'friend_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        //check info of friend
        $friendId = $this->request->get('friend_id');
        $friendInfo = UsersInfo::where([
            '_id' => mongo_id($friendId)
        ])->first();
        if (empty($friendInfo)) {
            return $this->errorBadRequest(trans('users.account_not_exist'));
        }

        //get current user list friend
        $currentUser = UsersInfo::where([
            '_id' => mongo_id(Auth::getPayLoad()->get('user_id'))
        ])->first();
        if (empty($currentUser)) {
            return $this->errorBadRequest(trans('users.account_not_exist'));
        }

        /**
         * 0: Bạn bè
         * 1: Đã gửi yêu cầu kết bạn
         * 2: Chờ xác nhận (trường hợp user gửi yêu cầu kết bạn tới currentUser và chờ current xác nhận)
         * 3: Không phải bạn bè
         */
        $currentUserListFriend = $currentUser->list_friends;
        if (isset($currentUserListFriend)) {
            foreach ($currentUserListFriend as $item) {
                if ($friendId == $item) {
                    $friendListFriend = $friendInfo->list_friends;
                    if (isset($friendListFriend)) {
                        foreach ($friendListFriend as $item) {
                            if ($currentUser->_id == $item) {
                                return $this->successRequest(0);
                            }
                        }
                    }
                }
            }
        }

        $currentSent = UserFriendRequest::where([
            'sender' => Auth::getPayLoad()->get('user_id'),
            'receiver' => $friendId
        ])->first();
        if (!empty($currentSent)) {
            return $this->successRequest(1);
        }

        $friendSent = UserFriendRequest::where([
            'sender' => $friendId,
            'receiver' => Auth::getPayLoad()->get('user_id')
        ])->first();
        if (!empty($friendSent)) {
            return $this->successRequest(2);
        }

        return $this->successRequest(3);
    }
}
