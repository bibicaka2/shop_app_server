<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\DepartmentRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class DepartmentController extends Controller
{

    protected $userRepository;
    protected $departmentRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        UserRepository $userRepository,
        DepartmentRepository $departmentRepository,
        AuthManager $auth,
        Request $request
    )
        {
            $this->userRepository = $userRepository;
            $this->departmentRepository = $departmentRepository;
            $this->request = $request;
            $this->auth = $auth;
            parent::__construct();
        }
    
    public function create()
    {
        $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $name = $this->request->get('name');
        // $address = $this->request->get('address');
        $attributes = [        
            'name' => $this->request->get('name')
        ];
        $validator =$this->departmentRepository->create($attributes);
        return $this->successRequest($validator);
    }
    
    public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
            '_id'=>'required', 
        ]);
        $id = $this->request->get('_id');
        // Delete
        $department = Department::where('_id', mongo_id($id))->first();
        if (!empty($department)) {    
            if(!empty($this->request->get('name')))
            {
                $department->name=$this->request->get('name');
            }
            
            $department->save();
        }
        return $this->successRequest($department); 
    }
    
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $department=Department::where(['_id',mongo_id($id),])->first();
        if(!empty($department))
        {   
            $department->delete();
        }
        return $this->successRequest();
    }
    
    public function view()
    {
          $department = Department::all();
       return $this->successRequest($department);
    }
}