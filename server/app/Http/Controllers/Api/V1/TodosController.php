<?php
namespace App\Http\Controllers\Api\V1;

// use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\TodosRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Todos;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class TodosController extends Controller
{
    //protected $userRepository;
    protected $todoRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        // UserRepository $userRepository,
        TodosRepository $todoRepository,
        AuthManager $auth,
        Request $request
    )   {
           // $this->userRepository = $userRepository;
            $this->todoRepository = $todoRepository;
            $this->request = $request;
            $this->auth = $auth;
            parent::__construct();
    }
    public function create()
    {
        $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
            'prioty'=>'required', 
            'description'=>'required', 
            'duedate'=>'required', 
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
       
        $attributes = [  
            'name' => $this->request->get('name'),  
            'prioty' => $this->request->get('prioty'),  
            'description' => $this->request->get('description'),  
            'duedate' => $this->request->get('duedate'),  
          
        ];

        $todos =$this->todoRepository->create($attributes);
       // return $this->successRequest($branch->transform('for-detail'));
        return $this->successRequest($todos);

    }
     public function update() {
        // Input
         $validator = \validator::make($this->request->all(),[
             'name'=>'required', 
            'prioty'=>'required', 
            'description'=>'required', 
            'duedate'=>'required', 
        ]);
        $id = $this->request->get('_id');
        $todos = Todos::where('_id', mongo_id($id))->first();
        if (!empty($todos))
        { 
            if(!empty($this->request->get('address')))
                {
                    $todos->address=$this->request->get('address');
                }
                 if(!empty($this->request->get('prioty')))
                {
                    $todos->prioty=$this->request->get('prioty');
                }
                 if(!empty($this->request->get('description')))
                {
                    $todos->description=$this->request->get('description');
                }
                 if(!empty($this->request->get('duedate')))
                {
                    $todos->duedate=$this->request->get('duedate');
                }
            $todos->save();
        }
         return $this->successRequest($todos);
    }
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
        ]);
        $id=$this->request->get('_id');
        $todos=Todos::where('_id',mongo_id($id))->first();
        if(!empty($todos))
        {
            $todos->delete();
        }
        return $this->successRequest();
    }
    public function view()
    {
        $todos = Todos::get();
        $viewData = [
            'Todos' => $todos
        ];
        return $viewData;
    }


}