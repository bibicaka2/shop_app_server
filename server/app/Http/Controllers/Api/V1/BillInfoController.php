<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\ProductsRepository;
use App\Api\Repositories\Contracts\BillInfoRepository;
use App\Api\Entities\UsersInfo;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;
use App\Libraries\Gma\APIs\APIAuth;
use Illuminate\Support\Facades\Auth;
use App\Api\Entities\Products;
use App\Api\Entities\BillInfo;



class BillInfoController extends Controller
{

    protected $userRepository;
    protected $billInfoRepository;
  
    protected $request;

    public function __construct(   
        BillInfoRepository $billInfoRepository, 
         ProductsRepository $productsRepository, 
        Request $request
    ) 
        {
            $this->billInfoRepository = $billInfoRepository;
            $this->productsRepository = $productsRepository;
            $this->request = $request;
            parent::__construct();
        }
    
    public function create()
    {

        $validator = \validator::make($this->request->all(),[
          //  'user_id'=>'required',
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'address'=>'required',
            'product_detail'=>'required'
            
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $user_id = Auth::getPayLoad()->get('user_id');
        $userInfo = UsersInfo::where('_id',mongo_id($user_id))->first();
        if(empty($userInfo)){
            return $this->errorBadRequest(trans('user info is valid'));
        }
        $product_detail_list=$this->request->get('product_detail');
        //werb
        $name=$this->request->get('name');
        $address=$this->request->get('address');
        //$product_size=$this->request->get('product_size');
        $phone=$this->request->get('phone');
        $sale_off_id=$this->request->get('sale_off_id');
        $email=$this->request->get('email');
        //app

        foreach ($product_detail_list as $key1 => $product_detail) {
            $products=[];
            $size='';
            foreach ($product_detail as $key => $value) {
                
                if($key==='product_id')
                {

                        $products = Products::where('_id',mongo_id($value))->first();
                        
                }

                else if($key==='size' || $key==='quantity' )      
                {

                    if($key==='size'){
                        $size=$value;
                        
                    }
                    else{
                        for( $i=0; $i <count($products->product_size_list);$i++){
                           // dd($products->product_size_list[$i]['size']);
                            if($products->product_size_list[$i]['size']==$size)
                            {
                                //cach fix
                                //https://laracasts.com/discuss/channels/eloquent/indirect-modification-of-overloaded-element//
                               $a=$products->product_size_list;
                                (string)$a[$i]['inventory']=(int)$a[$i]['inventory']-(int)($value);
                                $products->quantity_sold= $products->quantity_sold +(int)($value);
                                 $products->product_size_list=$a;
                                //(String)((int)$products->product_size_list[$i]['inventory']-(int)$value);
                                if((int)$products->product_size_list[$i]['inventory']>=0)
                                {
                                    $products->save();
                                }
                                else{
                                    $product_detail_list[$key1][$key]='sold_out';
                                   // return $this->errorBadRequest('sold out!');
                                }
                            }
                           
                        }

                    }
                }  
            }
        }
        $attributes = [   
            'user_id'=>$user_id,    
            'name'=>$name,
            'phone'=>$phone,
            'address'=>$address,
            'email'=>$email,
           'product_detail'=>$product_detail_list,       
           
        ];
        //djson($attributes);
        $billInfo =$this->billInfoRepository->create($attributes);
        return $this->successRequest($billInfo);
    }
    
   
    
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $billInfo=BillInfo::where(['_id',mongo_id($id),])->first();
        if(!empty($billInfo))
        {   
            $billInfo->delete();
        }
        return $this->successRequest();
    }
      public function findBillById()
    {
        $user_id = Auth::getPayLoad()->get('user_id');

        $userInfo = UsersInfo::where('_id',mongo_id($user_id))->first();
        if(empty($userInfo)){
            return $this->errorBadRequest(trans('user info is valid'));
        }
       // $user_id=$this->request->get('user_id');
        $billInfo=BillInfo::where('user_id',$user_id)->get();
        djson($billInfo);
        if(empty($billInfo))
        {   
               return $this->successRequest('fail to find bill');
        }
        return $this->successRequest($billInfo);
    }
    
    
    public function list()
    {

       $billInfo = BillInfo::all();
       if(count($billInfo)>0)
       {
         foreach ($billInfo as $key => $value) {
             $product_list[]= $value;
                }
                return $product_list;
       }
       return [];
    }
}