<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\UsersInfo;
use Illuminate\Http\Request;
use App\Api\Entities\Users;
use App\Api\Entities\UserToken;
use App\Api\Repositories\Contracts\UsersRepository;
use App\Http\Controllers\Controller;
use App\Libraries\Gma\APIs\APIUpload;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthManager;

class UsersController extends Controller
{
    protected $request;
    protected $usersRepository;
    protected $auth;

    public function __construct(Request $request, UsersRepository $usersRepository, AuthManager $auth)
    {
        $this->request = $request;
        $this->usersRepository = $usersRepository;
        $this->auth = $auth;

        parent::__construct();
    }

    public function register()
    {
        dd('da vo');
        $user_name = 'cokhanh';
        $password = '1';

        $attr = [
            'user_name' => $user_name,
            'password' => $password
        ];
        $user = Users::create($attr);
        return $this->successRequest(trans('core.success'));
    }

    public function logIn()
    {

        $validator = \Validator::make($this->request->all(), [
            'user' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        //user có thể là số điện thoại hoặc gmail
        $user = $this->request->get('user');
        $password = $this->request->get('password');

        //check user name có tồn tại
        $params = [
            'phone' => $user,
        ];
        $user_list = $this->usersRepository->getUsersAccount($params);

        if (count($user_list) == 0) {
            $params = [
                'email' => $user,
            ];

            $user_list = $this->usersRepository->getUsersAccount($params);
        }

        if (count($user_list) == 0) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $is_valid_account = 0; //0: Là không có tài khoản, 1: Có tài khoản
        foreach ($user_list as $item) {
            if (password_verify($password, $item->password)) {
                $is_valid_account = 1;
                $user_record = $item;
            }
        }

        //Nếu User có tồn tại tại thì add user vào token.
        if ($is_valid_account == 1) {
            //Lấy các thông tin của user đăng nhập
            $user_info = UsersInfo::where([
                '_id' => mongo_id($user_record->user_id)
            ])->first();
            if (empty($user_info)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }

            // if ($user_info->phone_verified == 0) {
            //     $data = [];
            //     $data['user_name'] = $user_info->last_name . ' ' . $user_info->first_name;
            //     $data['phone_verified'] = false;
            //     $data['token'] = '';
            //     $data['user_avatar'] = '';
            //     $data['user_cover'] = '';
            //     $data['no_sign_profile'] = $user_record->no_sign_profile;

            //     return $this->successRequest($data);
            // }

            $customClaims = [];
            $customClaims['full_name'] = $user_record->fullname;
            $customClaims['user_id'] = $user_record->user_id;
         //   $customClaims['user_full_name'] = $user_info->last_name . ' ' . $user_info->first_name;
            $customClaims['phone'] = $user_info->phone;
            $customClaims['address']=$user_info->address;
            $customClaims['email'] = $user_info->email;
           // $customClaims['sex'] = $user_info->sex;

            $token = $this->auth->customClaims($customClaims)->fromUser($user_record);

            $attribute = [
                'user_id' => $user_record->user_id,
                'api_token' => $token,
            ];

            UserToken::create($attribute);  
            $user_record->updated_at = Carbon::now();
            $user_record->save();
            $data = [
                'full_name' => $user_info->fullname,
                'token' => $token,
                'email' =>$user_info->email,
                'address' =>$user_info->address,
                'phone' => $user_info->phone,
                //'phone_verified' => true,
               // 'no_sign_profile' => $user_record->no_sign_profile
            ];
            // //get user avatar
            // $params = [
            //     'type' => 'image',
            //     'user_id' => $user_info->_id,
            //     'option' => 'avatars'
            // ];
            // $avatarURI = APIUpload::getFileToClient($params);
            // $data['user_avatar'] = $avatarURI;

            // //get user cover
            // $params = [
            //     'type' => 'image',
            //     'user_id' => $user_info->_id,
            //     'option' => 'cover_picture'
            // ];
            // $coverURI = APIUpload::getFileToClient($params);
            // $data['user_cover'] = $coverURI;

            return $this->successRequest($data);
        }

        return $this->errorBadRequest(trans('auth.incorrect'));
    }

    public function logOut()
    {
        $validator = \Validator::make($this->request->all(), [
            'api_token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        //client truyền token lên kèm với bearer (bearer + '' + token)
        $user_id = Auth::getPayLoad()->get('user_id');
        $userToken = UserToken::where([
            'user_id' => $user_id,
            'api_token' => $this->request->get('api_token')
        ])->first();
        if (!empty($userToken)) {
            $userToken->delete();
            return $this->successRequest(trans('core.success'));
        }

        return $this->errorBadRequest([]);
    }
    public function validateLogin()
    {
        $validator = \Validator::make($this->request->all(), [
            'api_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $user = Auth::user();
        $userToken = UserToken::where(['user_id' => $user->user_id, 'api_token' => $this->request->get('api_token')])->first();
        if (!empty($userToken)) {
            return $this->successRequest(trans('core.success'));
        }
        return $this->errorBadRequest([]);
    }

    public function getUserBasicInfo()
    {
        $userInfoList = UsersInfo::all();
        $data = [];

        foreach ($userInfoList as $item) {
            $data[] = $item->transform('get-all');
        }
        return $this->successRequest($data);
    }
}
