<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\ChangLanRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\ChangLan;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;

use Resources\lang\en\test;
class ChangLanController extends Controller
{
	protected $userRepository;
	protected $changlanRepository;
	protected $auth;
    protected $request;
    protected $abc;

    public function __construct(   
    	UserRepository $userRepository,
        ChangLanRepository $changlanRepository,
        AuthManager $auth,
        Request $request,
        test $abc
    ) {
            $this->userRepository = $userRepository;
            $this->changlanRepository = $changlanRepository;
            $this->request = $request;
            $this->auth = $auth;
            $this->abc=$abc;
            parent::__construct();
    }
	public function create()
    {
		$validator = \validator::make($this->request->all(),[
			'head'=>'required',
            'content'=>'required',
			'type'=>'required',			
		]);

		if($validator->fails()){
            
			return $this->errorBadRequest($validator->messages()->toArray());
		}
		
		$attributes = [		
			'content' => $this->request->get('content'),
			'type' => $this->request->get('type')
		];    
		$changlan =$this->changlanRepository->create($attributes);
        //return $this->successRequest(trans('auth.invalid_domain'));
	}
	
    public function test()
    {
        $lang->request->get('type');
        //dd( $a);
        if($lang=='vi')
        {
            $data1= app('translator')->setLocale('vi');    
        }
        else
        {
            $data1=app('translator')->setLocale('en'); 
        }
   
        $data=('auth.invalid_domain');
    	return $this->successRequest(trans($data));
    }

    public function test1()
    {
    //   $data= [
    // 'incorrect' => 'Email or Rassword is incorrect',
    // 'unactive' => 'Account has not been activated',
    // 'emptycode' => 'Can not found CODE',
    // 'activated' => 'User activated before',
    // 'active_require' => 'Please check your email to activate account',
    // 'active_success' => 'Account actived successfully',
    // 'not_enough_permission' => 'Permission denied',
    // 'invalid_domain' => 'Domain invalid',
    // ];
        
        foreach ($data as $key => $value) {
        $data1[]=$key;    
        $data2[]=$value;

        # code...
        }
         return $data1;
    }
}