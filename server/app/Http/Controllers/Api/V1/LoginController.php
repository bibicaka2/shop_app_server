<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\LoginRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Login;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    protected $userRepository;
    protected $loginRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        UserRepository $userRepository,
        LoginRepository $loginRepository,
        AuthManager $auth,
        Request $request
    )   {
            $this->userRepository = $userRepository;
            $this->loginRepository = $loginRepository;
            $this->request = $request;
            $this->auth = $auth;
            parent::__construct();
    }
    public function create()
    {

        $validator = \validator::make($this->request->all(),[
            'username'=>'required',
            'password'=>'required',
            'displayname'=>'required',
            'type'=>'required',
            'email'=>'required',
            'phone'=>'required',
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        // $name = $this->request->get('name');
        // $address = $this->request->get('address');
        // $phone = $this->request->get('phone');
        $attributes = [  
            'username' => $this->request->get('username'),
            'password' => $this->request->get('password'),
            'displayname' => $this->request->get('displayname'),
            'type'=>0,
            'email'=>$this->request->get('email'),
            'phone'=>$this->request->get('phone'),
        
        ];

        $login =$this->loginRepository->create($attributes);
        return $this->successRequest($login);
       

    }
     public function update() {
        // Input
         $validator = \validator::make($this->request->all(),[
            'username'=>'required',
            'password'=>'required',
            'displayname'=>'required',
            'type'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'name'=>'required',
          
        ]);
        $id = $this->request->get('_id');
        $login = Login::where('_id', mongo_id($id))->first();
        if (!empty($login))
        { 
            if(!empty($this->request->get('username')))
            {
                $login->username=$this->request->get('username');
            }
            if(!empty($this->request->get('password')))
            {
                $login->password=$this->request->get('password');
            }
            if(!empty($this->request->get('displayname')))
            {
                $login->displayname=$this->request->get('displayname');
            }
            if(!empty($this->request->get('phone')))
            {
                $login->phone = $this->request->get('phone');
            }
             if(!empty($this->request->get('email')))
            {
                $login->email = $this->request->get('email');
            }
           


            $login->save();
        }
         return $this->successRequest($login);
    }
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
        ]);
        $id=$this->request->get('_id');
        $login=Login::where('_id',mongo_id($id))->first();
        if(!empty($login))
        {
            $login->delete();
        }
        return $this->successRequest();
    }
    // public function view()
    // {
    //     $login = Login::get();
    //     $viewData = [
    //         'Login' => $login
    //     ];
    //     return $viewData;
    // }
    public function view()
    {
        
        $id_user = $this->request->get('id_user');
        $type_user=Login::where('_id',mongo_id($id_user))->where('type','=','1')->first();
        //djson($type_user);
        if(!empty($type_user))
        {
            //dd('1');
            $user=Login::get();
             foreach ($user as $key => $value) {
                $user_transform[]= $value->transform('user_list');
            }
            return [
                'id_user_view'=>$id_user,
                'list_view_user'=>$user_transform
            ];
        }
        else
        {
            $type_user_1=Login::where('_id',mongo_id($id_user))->first();
            return $this->successRequest($type_user_1);
        }
    }
    public function login_1()
    {
          $validator = \validator::make($this->request->all(),[
            'username'=>'required',
            'password'=>'required',
        ]);
          $login= Login::where('username',$this->request->get('username'))->where('password',$this->request->get('password'))->first();
         
          if(!empty($login))
          {
            dd('dangnhap thanh cong type cua ban là :'.$login->type);
          }
          else
          {
            dd('vui long nhap lai');
          }

    }  
}