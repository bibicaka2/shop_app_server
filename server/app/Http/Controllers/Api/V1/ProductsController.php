<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\ProductsRepository;
use App\Api\Repositories\Contracts\ProductSizeRepository;
use App\Api\Repositories\Contracts\ProductStatusRepository;
use App\Api\Repositories\Contracts\SaleOffRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;
use App\Api\Entities\SaleOff;
use App\Api\Entities\ProductStatus;
use App\Api\Entities\ProductSize;
use App\Api\Entities\Products;
use App\Api\Entities\ProductType;
use Illuminate\Validation\Rule;




class ProductsController extends Controller
{

    protected $userRepository;
    protected $productsRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        ProductsRepository $productsRepository, 
        Request $request
    )
        {
            $this->productsRepository = $productsRepository;
            $this->request = $request;
            parent::__construct();
        }
    
    public function create()
    {
        //rang buoc
        $validator = \validator::make($this->request->all(),[   
            'name'=>'required', 
            'img'=>'required', 
            //'status_id'=>'required', 
            'gender'=>'required',
            'description'=>'required',
            'product_size_list'=>'required',
            'rating'=>'required',
            //'sale_id'=>'required',
            'color'=>'required',
            'product_type_id'=>'required',
            'price_origin'=>'required',
          //  'quantity_sold'=>'required',
            //'price_after_sale_off'=>'required'


        ]);
         if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        // if($validator->fails()){
        //     return $this->errorBadRequest($validator->messages()->toArray());       
        // }
        //      if(!empty($this->request->get('name')))
        // {
        //    $name = $this->request->get('name');
        // }
        // if(!empty($this->request->get('img')))
        // {
        //    $img =  $this->request->get('img');
        // }
        // if(!empty($this->request->get('status_id')))
        // {
        //      $status_id =  $this->request->get('status_id');
        // }
        // if(!empty($this->request->get('description')))
        // {
        //       $description = $this->request->get('description');
        // }
        // if(!empty($this->request->get('inventory')))
        // {
        //     $inventory =  $this->request->get('inventory'); 
        // }
        // if(!empty($this->request->get('gender')))
        // {
        //      $gender =  $this->request->get('gender');   
        // }
        //  if(!empty($this->request->get('rating')))
        // {
        //      $rating =  $this->request->get('rating');   
        // }
        // if(!empty($this->request->get('sale_id')))
        // {
        //      $sale_id =  $this->request->get('sale_id');
        // }
        // if(!empty($this->request->get('product_size_list')))
        // {
        //     $product_size_list=$this->request->get('product_size_list');
        // }
        // if(!empty($this->request->get('color')))
        // {
        //      $color = $this->request->get('color');
        // }
        // if(!empty($this->request->get('product_type_id')))
        // {
        //      $product_type_id =  $this->request->get('product_type_id');
        // }
        // if(!empty($this->request->get('size_id')))
        // {
        //     $size_id =  $this->request->get('size_id');
        // }
        // if(!empty($this->request->get('price_origin')))
        // {
        //     $price_origin =  $this->request->get('price_origin');
        // }
        // if(!empty($this->request->get('price_after_sale_off')))
        // {
        //     $price_after_sale_off =  $this->request->get('price_after_sale_off');
        // }

        $name = $this->request->get('name');
        $img =  $this->request->get('img');
        $status_id =  $this->request->get('status_id');
        $description = $this->request->get('description');
        $gender = $this->request->get('gender');
        $inventory =  $this->request->get('inventory');        
        $rating = $this->request->get('rating');
        $sale_id =  $this->request->get('sale_id');
        $color = $this->request->get('color');
        $product_type_id =  $this->request->get('product_type_id');
        // }
        //check product Type
        if(strlen( $product_type_id )!=24){
                return $this->errorBadRequest(trans('product tyze is error'));
        }
        $product_type = ProductType::where('_id', mongo_id($product_type_id))->first(); 
        if($product_type_id==null){
             return $this->errorBadRequest(trans('product type is not found in list'));
        }

        //----------------------------------------------------------------------------//
        //----------------------------------------------------------------------------//        //
        //----------------------------------------------------------------------------//
        $price_origin =  $this->request->get('price_origin');
        $price_after_sale_off =  $this->request->get('price_after_sale_off');
        $product_size_list= $this->request->get('product_size_list');
      

        $attributes = [        
            'name'=>$name, 
            'img'=>$img,         
            'status_id'=>$status_id, 
            'description'=>$description,
            'rating'=>0,  
            'sale_id'=>$sale_id,
            'gender'=>$gender,
            'color'=>$color,
            'product_type_id'=>$product_type_id,
            'product_size_list'=>$product_size_list,
            'price_origin'=>$price_origin,
            'price_after_sale_off'=>$price_after_sale_off,
            'quantity_sold'=>0,
           
        ];
        $products =$this->productsRepository->create($attributes);
        return $this->successRequest($products);
    }
    
    public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
             'name'=>'required', 
            'img'=>'required', 
            'status_id'=>'required', 
            'gender'=>'required',
            'description'=>'required',
            'product_size_list'=>'required',
            'rating'=>'required',
            'sale_id'=>'required',
            'color'=>'required',
            'product_type_id'=>'required',
            'price_origin'=>'required',
            'price_after_sale_off'=>'required'
        ]);
         if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $id = $this->request->get('_id');
        $Products = Products::where([
            '_id'=> mongo_id($id),
        ])->first();
        if(!empty($this->request->get('name')))
        {
           $name = $this->request->get('name');
        }
        if(!empty($this->request->get('img')))
        {
           $img =  $this->request->get('img');
        }
        if(!empty($this->request->get('status_id')))
        {
             $status_id =  $this->request->get('status_id');
        }
        if(!empty($this->request->get('description')))
        {
              $description = $this->request->get('description');
        }
         if(!empty($this->request->get('inventory')))
        {
                 $inventory =  $this->request->get('inventory'); 
        }
        if(!empty($this->request->get('gender')))
        {
             $gender =  $this->request->get('gender');   
        }
         if(!empty($this->request->get('rating')))
        {
             $rating =  $this->request->get('rating');   
        }
        if(!empty($this->request->get('sale_id')))
        {
             $sale_id =  $this->request->get('sale_id');
        }
        if(!empty($this->request->get('product_size_list')))
        {
            $product_size_list=$this->request->get('product_size_list');
        }
        if(!empty($this->request->get('color')))
        {
             $color = $this->request->get('color');
        }
        if(!empty($this->request->get('product_type_id')))
        {
             $product_type_id =  $this->request->get('product_type_id');
        }
        if(!empty($this->request->get('size_id')))
        {
            $size_id =  $this->request->get('size_id');
        }
         if(!empty($this->request->get('price_origin')))
        {
            $price_origin =  $this->request->get('price_origin');
        }
            if(!empty($this->request->get('price_after_sale_off')))
        {
            $price_after_sale_off =  $this->request->get('price_after_sale_off');
        }
    
        
       
    
        $products = Products::where('_id', mongo_id($id))->first();

        if (!empty($products)) {    
            if(!empty($this->request->get('name')))
            {
                $products->name=$this->request->get('name');
            }
            if(!empty($this->request->get('img')))
            {
                $products->img=$this->request->get('img');
            }
            if(!empty($this->request->get('status_id')))
            {
                $products->status_id=$status_id;
            }
            if(!empty($this->request->get('description')))  
            {
            $products->description=$this->request->get('description');
            }
            if(!empty($this->request->get('gender')))
            {
                $products->gender=$this->request->get('gender');
            }
            // if(!empty($this->request->get('inventory')))
            // {
            //     $products->inventory=$this->request->get('inventory');
            // }
            if(!empty($this->request->get('rating')))
            {
                $products->rating=$this->request->get('rating');
            }
            if(!empty($this->request->get('sale_id')))
            {
                $products->sale_id=$this->request->get('sale_id');
            }
             if(!empty($this->request->get('product_size_list')))
            {
                $products->product_size_list=$this->request->get('product_size_list');
            }
            if(!empty($this->request->get('color')))
            {
                $products->color=$this->request->get('color');
            }
            if(!empty($this->request->get('product_type_id')))
            {
                $products->product_type_id=$this->request->get('product_type_id');
            }
          
            if(!empty($this->request->get('price_origin')))
            {
                $products->price_origin=$this->request->get('price_origin');
            }
             if(!empty($this->request->get('price_after_sale_off')))
            {
                $products->price_after_sale_off=$this->request->get('price_after_sale_off');
            }
            
            $products->save();
        }
        return $this->successRequest($products); 
    }
    
    public function delete()
    {

        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
         if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $id=$this->request->get('_id');
        $products=Products::where('_id',mongo_id($id))->first();
        if(!empty($products))
        {   
            $products->forceDelete();
        }
        return $this->successRequest();
    }
    public function list()
    {

          $validator = \Validator::make($this->request->all(), [
            // 'user_id'=>'required',
            // 'email'=>'email|unique:users_info',
            // 'phone'=>'unique:users_info|max:11|min:10',
            'list_type' => [
                'required',
                Rule::in([0, 1])

            //0 : list bth
            //1 : list ban duoc nhieu nhat
              
            ],
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $list_type = $this->request->get('list_type');
        if($list_type==0){
             
                $products = Products::get();
                $viewData = [
                    'data' => $products,
                    'message'=>'success'
                ];
                 return $viewData;
        }
        if($list_type==1){
                $products = Products::OrderBy('quantity_sold','desc')->get();
                $viewData = [
                    'data' => $products,
                    'message'=>'success'
                ];
                 return $viewData;
            
        }

        
    }
    public function GetProductByType(){
        $validator = \validator::make($this->request->all(),[
            'product_type_id'=>'required', 
        ]);
        if ($validator->fails()) {
         return $this->errorBadRequest($validator->messages()->toArray());
        }

        $product_type_id = $this->request->get('product_type_id');
       $products = Products::where('product_type_id',$product_type_id)->get();
       if($products)
       return $products;
        return $this->errorBadRequest('products is valid');
    }
    public function findProduct()
    {
   
        $validator = \validator::make($this->request->all(),[
            'id'=>'required', 
           
        ]);
        $id = $this->request->get('_id');
       $products = Products::where('_id',mongo_id($id))->first();
       if($products)
       {
        $viewData = [
            'data' => $products,
            'message'=>'success'
        ];
        return $viewData;
       }
        return $this->errorBadRequest('products is valid');
    }
    public function listProductByType(){
        $products=[];
        $validator =   $validator = \validator::make($this->request->all(),[
            'id'=>'required',   
        ]);

        $products = Products::get();
        $viewData = [
            'data' => $products,
            'message'=>'success'
        ];
        return $viewData;
    }
}