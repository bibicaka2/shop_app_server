<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\TimeKeepingRepository;  
use App\Api\Repositories\Contracts\StaffRepository;   
use App\Api\Repositories\Contracts\LoginRepository;
use App\Api\Entities\Login;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\TimeKeeping;
use App\Api\Entities\Staff;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;
use App\Api\Repositories\Contracts\ShiftWorkRepository;
use App\Api\Entities\ShiftWork;

use App\Api\Repositories\Contracts\SabbaticalLeaveRepository;
use App\Api\Entities\SabbaticalLeave;
class TimeKeepingController extends Controller
{
	protected $userRepository;
	protected $timekeepingRepository;
	protected $auth;
    protected $request;
    protected $check_id_sw;
    public function __construct(   
    	UserRepository $userRepository,
        TimeKeepingRepository $timekeepingRepository,
        AuthManager $auth,
        Request $request
    ) {
        $this->userRepository = $userRepository;
        $this->timekeepingRepository = $timekeepingRepository;
        $this->request = $request;
        $this->auth = $auth;
        parent::__construct();
    }


	public function create(){
        
		$validator = \Validator::make($this->request->all(),[	   
			'timein',
            'timeout',
            'id_shift_work'=>'required',
            'id_SabbaticalLeave',
		]);
		if($validator->fails()){
			return $this->errorBadRequest($validator->messages()->toArray());
		}
	    

        $timein = $this->request->get('timein');
		$timeout = $this->request->get('timeout');
        $id_shift_work = $this->request->get('id_shift_work');
        //$status= date("H:i:s");
        //$status= 0;
        //$this->request->get('status')=1;
          
   
		$attributes = [
		    //'nameshop'=> $this->request->get('nameshop'),
			//'accountlogin'=> $this->request->get('accountlogin'),
			
            'timein' =>0,
            'timeout'=>0,
			'id_shift_work'=> $this->request->get('id_shift_work'),
            'status'=>0,
            'totaltime'=>0
        ];
		$timekeeping =$this->timekeepingRepository->create($attributes);
        return $this->successRequest($timekeeping);
	}
	 public function update() {
        // Input
        $validator = \Validator::make($this->request->all(),[      
            'timein',
            'timout',
            'id_shift_work'=>'required',
            '_id'=>'required',
            'totaltime',
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $id = $this->request->get('_id');
        $timekeeping = TimeKeeping::where([
            '_id'=> mongo_id($id),
        ])->first();
        if (!empty($timekeeping)) {
            // if(!empty($this->request->get('timein1'))){$timekeeping->name=$this->request->get('timein1');}
            //  if(!empty($this->request->get('timeout1'))){$timekeeping->name=$this->request->get('timeout1');}
            if(!empty($this->request->get('timein')))
                {
                    $timekeeping->name=$this->request->get('name');
                }   
            if(!empty($this->request->get('timeout')))
                {
                    $timekeeping->timeout=$this->request->get('timeout');
                }
            if(!empty($this->request->get('id_shift_work')))
                {
                    $timekeeping->id_shift_work=$this->request->get('id_shift_work');
                }
            $timekeeping->save();
        }
         return $this->successRequest($timekeeping);
    }

    public function delete()
    {
        $validator = \Validator::make($this->request->all(),[      
            '_id'=>'required',
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

    	$id=$this->request->get('_id');  	
    	$timekeeping=TimeKeeping::where('_id',mongo_id($id))->first();
    	
        if(!empty($timekeeping))
    	{		
    		$timekeeping->delete();
    	}
    	return $this->successRequest(trans('core.success'));
    }
    public function view()
    {

    	$timekeeping = Timekeeping::all();
       	$viewData = [
            'Timekeeping' => $timekeeping
        ];    
        return view('timekeeping-list',$viewData);
    }
    
    public function check()
    {
        //dd("aaa");
        $validator = \Validator::make($this->request->all(),[      
            'id_shift_work'=>'required',
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $id_shift_work=$this->request->get('id_shift_work');
        $timekeeping=TimeKeeping::where([
                'id_shift_work'=>$id_shift_work,
                'timeout'=>0,
            ])->first();
        // $timekeeping = TimeKeeping::where('_id',mongo_id ($id_timekeeping))->first();   

       // $hours=date('H',strtotime($timekeeping->timein));

        $shiftwork= ShiftWork::where('_id',$timekeeping->id_shift_work)->first();
            $month=date("m");
            $date = date("d");
            $hours = (int)date("H");
           // dd($hours);
            // $sabbaticalLeave= SabbaticalLeave::where([
            //     'id_shift_work'=>$id_shift_work,
            //     'date'=>(string)$date,
            //     'month'=>(string)$month,
                    
            // ])->where('time_from','<',$hours)->where('time_to','>',$hours)->first();
            // dd($sabbaticalLeave);
         
        if($timekeeping->status ==0)
        {       
            $timein_check=date("H:i:s");
            // $month=date("m");
            // $date = date("d");
            // $sabbaticalLeave= SabbaticalLeave::where([
            //     'id_shift_work'=>$id_shift_work,
            //     'date'=>(string)$date,
            //     'month'=>(string)$month,
            // ])->first();
            
            
            if(date("H",strtotime($timein_check))>=7 &&date("H",strtotime($timein_check)) <= 17 )
            {
              
                $timekeeping->status=1;
                $timekeeping->timein= $timein_check;            
                $timekeeping->save();   
                 return $this->successRequest($timekeeping);  
            }
            else
            {
                dd("da xin nghi phep hoac qua gio cham cong");
            }


            
        }
        else if($timekeeping->status ==1)
        {
           
            $hours_1=date("H",strtotime($timekeeping->timein));
          
            $timeout_check=date("H:i:s");
           
            //-------A-----------------B---------
            //------>A---------------->B---------
            $sabbaticalLeave= SabbaticalLeave::where([
                'id_shift_work'=>$id_shift_work,
                'date'=>(string)$date,
                'month'=>(string)$month,
                    
            ])->where('time_from','<',(int)date("H",strtotime($timekeeping->timein)))->where('time_to','<',$hours)->first();

            //-------A-----------------B---------
            //------<A---------------->B---------
            $sabbaticalLeave1= SabbaticalLeave::where([
                'id_shift_work'=>$id_shift_work,
                'date'=>(string)$date,
                'month'=>(string)$month,
                    
            ])->where('time_from','>',(int)date("H",strtotime($timekeeping->timein)))->where('time_to','<',(int)date("H",strtotime($timeout_check)))->first();
              
            
            //dd($sabbaticalLeave1);
            //-------A-----------------B---------
            //------>A----------------<B---------
            $sabbaticalLeave2= SabbaticalLeave::where([
                'id_shift_work'=>$id_shift_work,
                'date'=>(string)$date,
                'month'=>(string)$month,
                    
            ])->where('time_from','<',(int)date("H",strtotime($timekeeping->timein)))->where('time_to','>',$hours)->first();

            //-------A-----------------B---------
            //------<A----------------<B---------

              $sabbaticalLeave3= SabbaticalLeave::where([
                'id_shift_work'=>$id_shift_work,
                'date'=>(string)$date,
                'month'=>(string)$month,
                    
            ])->where('time_from','>',(int)date("H",strtotime($timekeeping->timein)))->where('time_to','>',$hours)->first();
            //$timeout_check=date("H:i:s");
              // dd($sabbaticalLeave1);
            if(date("H",strtotime($timeout_check))>=11 &&date("H",strtotime($timeout_check))<=17 )
            {
                $timekeeping->timeout= $timeout_check; 
                if(empty($sabbaticalLeave) && empty($sabbaticalLeave1) && empty($sabbaticalLeave2) && empty($sabbaticalLeave3) )
                {
                    //dd('0');
                    //$timekeeping_total=date("H",strtotime($timekeeping->timeout))-date("H",strtotime($timekeeping->timein));
                    if(date("H",strtotime($timekeeping->timein)) <8)
                    {
                        
                        $hours = date("H",strtotime($timekeeping->timeout))-8;
                        $minute =date("i",strtotime($timekeeping->timeout));
                        $date=mktime($hours,$minute);
                        $timekeeping_total= date('H:i',$date);
                    
                       
                        $timekeeping->totaltime=$timekeeping_total;                 
                        $timekeeping->save();
                    }
                    if(date("H",strtotime($timekeeping->timein))>= 8 )
                    {
                        
                        $hours = date("H",strtotime($timekeeping->timeout))-date("H",strtotime($timekeeping->timein));
                        $minute =date("i",strtotime($timekeeping->timeout))-date("i",strtotime($timekeeping->timein));
                        $date=mktime($hours,$minute);
                        $timekeeping_total= date('H:i',$date);
                        $timekeeping->totaltime=$timekeeping_total;                 
                        $timekeeping->save();
                    }
                    
                    $attributes = [
                    'timein' =>0,
                    'timeout'=>0,
                    'id_shift_work' =>$timekeeping->id_shift_work   ,              
                    'status'=>0
                    //'status'=>$status
                    ];
                    $timekeeping_2 =$this->timekeepingRepository->create($attributes);
                    $timekeeping->status=11;
                    $timekeeping->save();

                    return $this->successRequest($timekeeping); 
                } 
                if(!empty($sabbaticalLeave))
                {        
                    // dd('1');
                    $hours = date("H",strtotime($timekeeping->timeout))-$sabbaticalLeave->time_to;
                    $minute =date("i",strtotime($timekeeping->timeout))-0;
                    $date=mktime($hours,$minute);
                    $timekeeping_total= date('H:i',$date);
                    $timekeeping->totaltime=$timekeeping_total;                 
                    $timekeeping->save();
                    $attributes = [
                        'timein' =>0,
                        'timeout'=>0,
                        'id_shift_work' =>$timekeeping->id_shift_work   ,              
                        'status'=>0
                            //'status'=>$status
                        ];
                        $timekeeping_2 =$this->timekeepingRepository->create($attributes);
                        $timekeeping->status=11;
                        $timekeeping->save();
                        return $this->successRequest($timekeeping);  
                
                }
                if(!empty($sabbaticalLeave1))
                {
                   // dd('2');
                    if(date("H",strtotime($timekeeping->timeout))>=11 && date("H",strtotime($timekeeping->timeout))<=17)
                    {
                            
                        if(date("H",strtotime($timekeeping->timein)) <=8)
                        {
                            
                            $hours = date("H",strtotime($timekeeping->timeout))-8 - ($sabbaticalLeave1->time_to - $sabbaticalLeave1->time_from);
                            $minute =date("i",strtotime($timekeeping->timeout)) - date("i",strtotime($timekeeping->timein)) ;
                            $date=mktime($hours,$minute);
                            $timekeeping_total= date('H:i',$date);
                           
                               
                            $timekeeping->totaltime=$timekeeping_total;                 
                            $timekeeping->save();
                        }
                     

                        if(date("H",strtotime($timekeeping->timein))> 8 )
                        {
                        
                            $hours = date("H",strtotime($timekeeping->timeout))-date("H",strtotime($timekeeping->timein))-($sabbaticalLeave1->time_to - $sabbaticalLeave1->time_from);
                            $minute =date("i",strtotime($timekeeping->timeout))-date("i",strtotime($timekeeping->timein));
                            $date=mktime($hours,$minute);
                            $timekeeping_total= date('H:i',$date);
                            $timekeeping->totaltime=$timekeeping_total;                 
                            $timekeeping->save();
                        }
                        $attributes = [
                            'timein' =>0,
                            'timeout'=>0,
                            'id_shift_work' =>$timekeeping->id_shift_work   ,              
                            'status'=>0
                            //'status'=>$status
                        ];
                        $timekeeping_2 =$this->timekeepingRepository->create($attributes);
                        $timekeeping->status=11;
                        $timekeeping->save();
                        return $this->successRequest($timekeeping); 
                    }         
                }
                if(!empty($sabbaticalLeave2))
                {
                    //dd('3');

                    $timekeeping_total= 0;
                            $timekeeping->totaltime=$timekeeping_total;                 
                            $timekeeping->save(); 
                    $attributes = [
                            'timein' =>0,
                            'timeout'=>0,
                            'id_shift_work' =>$timekeeping->id_shift_work   ,              
                            'status'=>0
                            //'status'=>$status
                            ];
                            $timekeeping_2 =$this->timekeepingRepository->create($attributes);
                            $timekeeping->status=11;
                            $timekeeping->save();
                            return $this->successRequest($timekeeping); 
                }
                if(!empty($sabbaticalLeave3))
                {
                   //dd('4');
                    if(date("H",strtotime($timekeeping->timein)) <8)
                    {
                          //  dd('4.2');
                        $hours = $sabbaticalLeave3->time_from- 8;
                        $minute=0;
                        $date=mktime($hours,$minute);
                        $timekeeping_total= date('H:i',$date);
                           
                               
                        $timekeeping->totaltime=$timekeeping_total;                 
                        $timekeeping->save();
                    }
                     

                    if(date("H",strtotime($timekeeping->timein))>= 8 )
                    {
                       
                        $hours = $sabbaticalLeave3->time_from-date("H",strtotime($timekeeping->timein));
                       
                        $minute =0-date("i",strtotime($timekeeping->timein));
                        $date=mktime($hours,$minute);
                        $timekeeping_total= date('H:i',$date);
                        $timekeeping->totaltime=$timekeeping_total;                 
                        $timekeeping->save();
                    }
                    $attributes = [
                        'timein' =>0,
                        'timeout'=>0,
                        'id_shift_work' =>$timekeeping->id_shift_work  ,              
                        'status'=>0
                        //'status'=>$status
                        ];
                    $timekeeping_2 =$this->timekeepingRepository->create($attributes);
                    $timekeeping->status=11;                            
                    $timekeeping->save();
                    return $this->successRequest($timekeeping); 
                }
            }
            // if(date("H",strtotime($timeout_check))>16 && date("H",strtotime($timeout_check))<19)
            // {
                
            //     if(empty($sabbaticalLeave))
            //     {
            //         $timekeeping->timeout= $timeout_check; 
            //         //$timekeeping_total=date("H",strtotime($timekeeping->timeout))-date("H",strtotime($timekeeping->timein));
            //         if(date("H",strtotime($timekeeping->timein)) <8)
            //         {
                        
            //             $hours = 16-8;
            //             $minute =0-0;
            //             $date=mktime($hours,$minute);



            //             $timekeeping_total= date('H:i',$date);
            //             $timekeeping->totaltime=$timekeeping_total;                 
            //             $timekeeping->save();
            //         }
            //         if(date("H",strtotime($timekeeping->timein))>= 8 )
            //         {
                        
            //             $hours = 16-date("H",strtotime($timekeeping->timein));
            //             $minute =0-date("i",strtotime($timekeeping->timein));
            //             $date=mktime($hours,$minute);
            //             $timekeeping_total= date('H:i',$date);
            //              $timekeeping->totaltime=$timekeeping_total;   
            //             $timekeeping->save();
            //         }
                    
                    
            //         $attributes = [
            //         'timein' =>0,
            //         'timeout'=>0,
            //         'id_shift_work' =>$timekeeping->id_shift_work ,              
            //         'status'=>0
            //         //'status'=>$status
            //         ];
            //         $timekeeping_2 =$this->timekeepingRepository->create($attributes);
            //         $timekeeping->status=11;
            //         $timekeeping->save();

            //         return $this->successRequest($timekeeping);  
            //     }
            //     else
            //     {
                    
                        
            //             $timekeeping->timeout= $timeout_check; 
            //             if($sabbaticalLeave->time_to>$timekeeping->timeout)
            //             {
            //                  $timekeeping->totaltime=0;  
            //                  $timekeeping->save();  
            //             }
            //             else if($timekeeping->timeout>=15 &&  $timekeeping->timeout<=17)
            //             {
            //                 $timekeeping->totaltime=$timekeeping->timeout - $timekeeping->timein;
            //                  $timekeeping->save(); 
            //             }
                                  
            //                  $attributes = [
            //                 'timein' =>0,
            //                 'timeout'=>0,
            //                 'id_shift_work' =>$timekeeping->id_shift_work   ,              
            //                 'status'=>0
            //                 //'status'=>$status
            //                 ];
            //                 $timekeeping_2 =$this->timekeepingRepository->create($attributes);
            //                 $timekeeping->status=1;
            //                 $timekeeping->save();
            //                 return $this->successRequest($timekeeping);  
                    
            //     }
            // }
           
        }
           // return $this->errorBadRequest();
    }

    public function view_shift_work()
    {
        $params = [  
            'id_shift_work' => $this->request->get('id_shift_work'),
            'status'=>11,
        ];
        
        $timekeeping=$this->timekeepingRepository->getTimeKeeping($params);
        return $this->successRequest($timekeeping);  
    }   
    public function view_shift_work_1()
    { 
        $validator = \Validator::make($this->request->all(),[      
            'id_user'=>'required',
            'id_shift_work' => 'required',
        ]);
        if($validator->fails()){    
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $id_user = $this->request->get('id_user');
        $type_user=Login::where('_id',mongo_id($id_user))->where('type','=','1')->first();
        $type_user_1=Login::where('type','=','1')->get();
       // djson($type_user);
        if(!empty($type_user))
        {
            $id_shift_work=$this->request->get('id_shift_work');
            $timekeeping = Timekeeping::where('id_shift_work',$id_shift_work)->get();
            djson($timekeeping);
            foreach ($timekeeping as $key => $value) {
                $timekeeping_transform[]= $value->transform('TimeKeeping_list');
            }   
            foreach ($type_user_1 as $key => $value) {
                $type_user_1_transform[]= $value->transform('user_view');
            }
            return [
                'id_shift_work'=>$id_shift_work,
                'history'=>$timekeeping_transform,
                'userviewing'=>$type_user->transform('user_view'),
                'userview'=>$type_user_1_transform,
                
            ];
        }
        else
        {
            dd('khong duoc quyen xem');
        }
    }   
    public function view_shift_work_2()
    { 
         $validator = \Validator::make($this->request->all(),[      
            'id_user'=>'required',
           // 'id_shift_work' => 'required',
        ]);
        if($validator->fails()){    
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $id_user = $this->request->get('id_user');
        $type_user=Login::where('_id',mongo_id($id_user))->where('type','=','1')->first();
        $type_user_1=Login::where('type','=','1')->get();
       // djson($type_user);
        if(!empty($type_user))
        {
           // $id_shift_work=$this->request->get('id_shift_work');
            $timekeeping = Timekeeping::get();   
            foreach ($timekeeping as $key => $value) {
                $shiftwork_id =ShiftWork::where('_id',mongo_id($value->id_shift_work))->first();           
                $id_staff_1=Staff::where('_id',mongo_id($shiftwork_id->id_staff))->first();
                $id_staff_2= $id_staff_1->transform('info');
                $timekeeping_transform[]= $value->transform('TimeKeeping_list',$id_staff_2);
              
                //$timekeeping_transform[$id_staff_2];
               
            }
            foreach ($type_user_1 as $key => $value) {
                $type_user_1_transform[]= $value->transform('user_view');
            }
            $data= [
                'View_TimeKeeping'=>$timekeeping_transform, 
            ];
            return $data;
        }
        else
        {
            dd('khong duoc quyen xem');
        }
    }   
    public function test_2()
    {


        $a = date('d/m/Y H:i:s');
      $time1 = date('8:44:01');
       //  $time2 = date('15:45:33')
     $time6= date('y:m:d');
       //  //$time3 = Carbon::Parse();
       //  //date("H",strtotime($timeout_check))
         $time4= Carbon::Parse()->format('yy/m/d h:i:s');
        $time7 = Carbon::now();

           //  $time5=(int)date("H",strtotime($time4));
       //  $hours = date("H",strtotime($time2))-date("H",strtotime($time1));
       //  $minute =date("i",strtotime($time2))-date("i",strtotime($time1));
       //  $date=mktime($hours,$minute);
       //  $time3= date('H:i',$date);
       //  djson($time3);
         djson($time4);
     
    }


    public function test_3()
    {
        // //dd('aaa');
        // $value=9000000;
        // $value1=2000000;
        // if(is_numeric($value) ){
        //         $total=number_format($value, 0, '.', ',');
        //     }
        $number=20000000;
         $fractional=false;
        if ($fractional) {  
            $number = sprintf('%.2f', $number);  
        }  
        while (true) {  
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);  
            if ($replaced != $number) {  
                $number = $replaced;  
            } else {  
                break;  
            }  
        }  
        return $number;  
 
    }
    public function test_4()
    {
        // //dd('aaa');
        // $value=9000000;
        // $value1=2000000;
        // if(is_numeric($value) ){
        //         $total=number_format($value, 0, '.', ',');
        //     }
       $time = mktime(14,36);
       $time1 = date('H:i',$time);
       dd($time);
 
    }

}