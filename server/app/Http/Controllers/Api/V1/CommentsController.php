<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\CommentsRepository;
use App\Api\Repositories\Contracts\LoginRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Comments;
use App\Api\Entities\Login;
use App\Libraries\Gma\APIs\APIAuth;
use App\Api\Entities\User;
use App\Api\Entities\Products;
use App\Api\Entities\UsersInfo;
use Illuminate\View\View;

use Illuminate\Support\Facades\Auth;


class CommentsController extends Controller
{

	protected $commentsRepository;
    protected $loginRepository;
	protected $auth;
    protected $request;

    public function __construct(   
        CommentsRepository $commentsRepository,
        LoginRepository $loginRepository,
        Request $request
    ) { 
        $this->commentsRepository =  $commentsRepository;
        $this->loginRepository = $loginRepository;
        $this->request = $request;
        parent::__construct();
    }
	public function create()
    {
		$validator = \validator::make($this->request->all(),[
		//	'content'=>'required',		
            'star_vote'=>'required',
            'product_id'=>'required',	
		]);
		if($validator->fails()){ 
			return $this->errorBadRequest($validator->messages()->toArray());
		}
        $user_id = Auth::getPayLoad()->get('user_id');
        $userInfo = UsersInfo::where('_id',mongo_id($user_id))->first();
        if(empty($userInfo)){
            return $this->errorBadRequest(trans('user info is valid'));
        }
        $star_vote = $this->request->get('star_vote');
        $content = $this->request->get('content');
        $product_id = $this->request->get('product_id');

        $check_comment = Comments::where(['user_id'=>$user_id,'product_id'=>$product_id])->first();
        //djson($check_comment);
       // dd(count($check_comment));
        if(empty($check_comment)){
            $comment = Comments::where('product_id',$product_id)->get();

            $product= Products::where('_id',mongo_id($product_id))->first();
          //  djson($product);
            if(empty($product)){
                    return $this->errorBadRequest('product is valid'); 
            }
            $total_star_hide=0;
            if(count($comment) ==0){
                $total_star_hide=$star_vote;
                $product->rating= (int)$star_vote;
            }
           else{
                 $comment_last = Comments::where('product_id',$product_id)->orderBy('total_star_hide','desc')->first();
                $total_star_hide= $comment_last->total_star_hide+$star_vote;
                $product->rating =  round(($total_star_hide/(count($comment)+1)), 3);
               // djson(count($comment));
               // djson($product->rating);
           }
          
           $product->save();
            $data= [
            'name'=>$userInfo->fullname,
            'content'=>$content,
            'star_vote'=>$star_vote,
            'date'=>  Carbon::Parse()->format('Y/m/d h:i:s'),
            ];
            $attributes = [     
            'user_id' => $user_id,
            'product_id'=>$product_id,
            'content' =>$content,
            'total_star_hide'=>(int)$total_star_hide,
            'star_vote'=>$star_vote,
            ];
            $comments =$this->commentsRepository->create($attributes);
            return $this->successRequest($data);
        }
        return $this->errorBadRequest('you commented');
     //  $attributes['user']=$user_comment;
       
	}
	 public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
            'user_id'=>'required',   
            'content'=>'required',         
        ]);
        $id = $this->request->get('_id');
        $company = Company::where([
            '_id'=> mongo_id($id),
        ])->first();
        if (!empty($company))
        {
            if(!empty($this->request->get('user_id')))
                {
                    $company->user_id=$this->request->get('user_id');
                }
            if(!empty($this->request->get('content')))
                {
                    $company->content=$this->request->get('content');
                }
            $company->save();
        }
         return $this->successRequest($company);
       
    }
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
                    
        ]);
    	$id=$this->request->get('_id');  	
    	$company=Comments::where('_id',mongo_id($id))->first();
    	if(!empty($company))
    	{	
    		$company->delete();
    	}
    	return $this->successRequest(trans('core.success'));
    }
    public function list()
    {
        $validator = \validator::make($this->request->all(),[
        //  'content'=>'required',      
        
            'product_id'=>'required',   
        ]);
        if($validator->fails()){ 
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $product_id= $this->request->get('product_id');

    	$comments = Comments::where('product_id',$product_id)->get();

        for( $i=0; $i <count($comments);$i++){
         $userInfo = UsersInfo::where('_id',$comments[$i]->user_id)->first();
            if(empty($userInfo)){
                return $this->errorBadRequest('user is valid');
            }
            else
            {
                $info_comment=[
                    'name'=>$userInfo->fullname,
                    'star_vote'=>$comments[$i]->star_vote,
                    'content'=>$comments[$i]->content,
                    'product_id'=>$comments[$i]->product_id,
                ];
                $data[]=$info_comment;
            }
        }
        djson($data);
       // $viewData = [
       //  'data' => $Comment,
       //  'message'=>'success'
       //  ];
        return $data;
       	
    }
}