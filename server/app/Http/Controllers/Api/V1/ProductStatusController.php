<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\ProductStatusRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;
use App\Api\Entities\ProductStatus;


class ProductStatusController extends Controller
{
    protected $productStatusRepository;
    protected $request;

    public function __construct(   
        ProductStatusRepository $productStatusRepository, 
        Request $request
    )
        {
            $this->productStatusRepository = $productStatusRepository;
            $this->request = $request;
            parent::__construct();
        }
    
    public function create()
    {

        $validator = \validator::make($this->request->all(),[
            'name'=>'required',
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        
        $name = $this->request->get('name');
       
       
        $attributes = [        
            'name' => $this->request->get('name'),      
        ];
        $productStatus =$this->productStatusRepository->create($attributes);
        return $this->successRequest($productStatus);
    }
    
    public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
          
        ]);
        $id = $this->request->get('_id');
       $status =  $this->request->get('status');
        $check = false;
        if($status ==='true' )
        {
            $check = true;
        }
        $productStatus = productStatus::where('_id', mongo_id($id))->first();
        if (!empty($productStatus)) {    
            if(!empty($this->request->get('name')))
            {
                $productStatus->name=$this->request->get('name');
            }
         
            
            $productStatus->save();
        }
        return $this->successRequest($productStatus); 
    }
    
    public function delete()
    {

        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $productStatus=ProductStatus::where('_id',mongo_id($id))->first();
        if(!empty($productStatus))
        {   
            $productStatus->forceDelete();
        }
        return $this->successRequest();
    }
    public function list()
    {

       $productStatus = ProductStatus::all();
       if(count($productStatus)>0)
       {
         foreach ($productStatus as $key => $value) {
             $product_list[]= $value;
                }
                return $product_list;
       }
       return [];
    }
    public function findProduct()
    {
        $productStatus=[];
        $validator = \validator::make($this->request->all(),[
            'key'=>'required',
        ]);
        $key=$this->request->get('key');
        $key=build_slug($key);

        $params=[
            'key'=> $this->request->get('key')
        ];
        $productStatus=$this->productStatusRepository->getBranch($params);
        return $productStatus;



        $validator = \validator::make($this->request->all(),[
            'id'=>'required', 
           
        ]);
        $id = $this->request->get('_id');
       $ProductStatus = ProductStatus::where('_id',mongo_id($id))->first();
       
       return $ProductStatus;
    }
    public function listProductByType(){
        $ProductStatus=[];
        $validator =   $validator = \validator::make($this->request->all(),[
            'id'=>'required',   
        ]);
    }
}