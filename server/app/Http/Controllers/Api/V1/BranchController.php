<?php
namespace App\Http\Controllers\Api\V1;

// use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\BranchRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Branch;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class BranchController extends Controller
{
    //protected $userRepository;
    protected $branchRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        // UserRepository $userRepository,
        BranchRepository $branchRepository,
        AuthManager $auth,
        Request $request
    )   {
           // $this->userRepository = $userRepository;
            $this->branchRepository = $branchRepository;
            $this->request = $request;
            $this->auth = $auth;
            parent::__construct();
    }
    public function create()
    {
        $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
            'skill'=>'required', 
            'education'=>'required', 
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
       
        $attributes = [  
            'name' => $this->request->get('name'),  
             'skill' => $this->request->get('skill'),  
              'education' => $this->request->get('education'),  
        ];

        $branch =$this->branchRepository->create($attributes);
       // return $this->successRequest($branch->transform('for-detail'));
        return $this->successRequest($branch);

    }
     public function update() {
        // Input
         $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
            'address'=>'required',
        ]);
        $id = $this->request->get('_id');
        $branch = Branch::where('_id', mongo_id($id))->first();
        if (!empty($branch))
        { 
            if(!empty($this->request->get('address')))
                {
                    $branch->address=$this->request->get('address');
                }
            $branch->save();
        }
         return $this->successRequest($branch);
    }
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
        ]);
        $id=$this->request->get('_id');
        $branch=Branch::where('_id',mongo_id($id))->first();
        if(!empty($branch))
        {
            $branch->delete();
        }
        return $this->successRequest();
    }
    public function view()
    {
        $branch = Branch::get();
        $viewData = [
            'Branch' => $branch
        ];
        return $viewData;
    }
    public function Test()
    {

        $branch = Branch::get();
        foreach ($branch as $key => $value) {
           $branch_transform[]= $value->transform('for-list');
        }
        // $viewData = [
        //     'Branch' => $branch_transform
        // ];
        return $branch_transform;
        //return $this->successRequest($branch ->transform('for-detail'));
    }
    public function Test_1()
    { 
       // dd('aaaaaa');   
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
        ]);
        $params = [  
            '_id' => $this->request->get('_id')
            //'is_detail'=>1
        ];
       $branch=$this->branchRepository->getBranch($params);

       return $branch;
    }
    public function Test_2()
    {
        $validator = \validator::make($this->request->all(),[
            'keyword'=>'required',
        ]);
        $keyword=$this->request->get('keyword');
        $keyword=build_slug($keyword);
        //dd($keyword);
        // $bundles=Branch::where('address','like',"%".$keyword."%");
        //  djson($bundles);
        //  return $this->successRequest($bundles);
        $params = [  
            'keyword' => $this->request->get('keyword')
            //'is_detail'=>1
        ];
       $branch=$this->branchRepository->getBranch($params);
       return $branch;
    }
    //tim kiem bang keyword
    public function test_3()
    {
        $validator = \validator::make($this->request->all(),[
            'key'=>'required',
        ]);
        $key=$this->request->get('key');
        $key=build_slug($key);

        $params=[
            'key'=> $this->request->get('key')
        ];
        $branch=$this->branchRepository->getBranch($params);
        return $branch;
    }
}