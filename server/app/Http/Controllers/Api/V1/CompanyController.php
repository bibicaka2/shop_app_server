<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\CompanyRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Company;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class CompanyController extends Controller
{           
	protected $userRepository;
	protected $companyRepository;
	protected $auth;
    protected $request;

    public function __construct(   
    	UserRepository $userRepository,
        CompanyRepository $companyRepository,
        AuthManager $auth,
        Request $request
    ) {
            $this->userRepository = $userRepository;
            $this->companyRepository = $companyRepository;
            $this->request = $request;
            $this->auth = $auth;
            parent::__construct();
    }
	public function create()
    {
		$validator = \validator::make($this->request->all(),[
			'name'=>'required',
			'id_branch'=>'required',			
		]);
		if($validator->fails()){
            return 
			return $this->errorBadRequest($validator->messages()->toArray());
		}
		$name= $this->request->get('name');
		
		$id_branch = $this->request->get('phone');
		$attributes = [		
			'name' => $this->request->get('name'),
			'id_branch' => $this->request->get('id_branch')
		];
		$company =$this->companyRepository->create($attributes);
        return $this->successRequest($company);
	}
	 public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
            'id_branch'=>'required',   
            'name'=>'required',         
        ]);
        $id = $this->request->get('_id');
        $company = Company::where([
            '_id'=> mongo_id($id),
        ])->first();
        if (!empty($company))
        {
            if(!empty($this->request->get('name')))
                {
                    $company->name=$this->request->get('name');
                }
            if(!empty($this->request->get('id_branch')))
                {
                    $company->id_branch=$this->request->get('id_branch');
                }
            $company->save();
        }
         return $this->successRequest($company);
       
    }
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
                    
        ]);
    	$id=$this->request->get('_id');  	
    	$company=Company::where('_id',mongo_id($id))->first();
    	if(!empty($company))
    	{	
    		$company->delete();
    	}
    	return $this->successRequest(trans('core.success'));
    }
    public function view()
    {
    	$company = Company::get();
        djson($company);
       	
    }
}