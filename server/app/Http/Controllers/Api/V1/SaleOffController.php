<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\SaleOffRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;
use App\Api\Entities\SaleOff;




class SaleOffController extends Controller
{

    protected $userRepository;
    protected $SaleOffRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        SaleOffRepository $saleOffRepository, 
        Request $request
    )
        {
            $this->saleOffRepository = $saleOffRepository;
            $this->request = $request;
            parent::__construct();
        }
    
    public function create()
    {

       $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
            'discount'=>'required',
            'from_date'=>'date',
            'to_date'=>'date',

        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $name = $this->request->get('name');
        $discount = $this->request->get('discount');
        $from_date =date('m/d/Y',strtotime($this->request->get('from_date'))); 
           $to_date =date('m/d/Y',strtotime($this->request->get('to_date'))); 
        $code = $this->request->get('code');
        $attributes = [        
            'name' =>$name,   
            'discount'=>$discount,
            'from_date'=>date($from_date),
            'to_date'=>date($to_date),
            'code'=>$code,
           
        ];
        $saleOff =$this->saleOffRepository->create($attributes);
        return $this->successRequest($saleOff);
    }
    
    public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
              'discount'=>'required',
            'from_date'=>'required ||date',
            'to_date'=>'required ||date'
          
        ]);
        $id = $this->request->get('_id');
      
        $saleOff = SaleOff::where('_id', mongo_id($id))->first();
        if (!empty($saleOff)) {    
            if(!empty($this->request->get('name')))
            {
                $saleOff->name=$this->request->get('name');
            }
             if(!empty($this->request->get('discount')))
            {
                $saleOff->discount=$this->request->get('discount');
            }
             if(!empty($this->request->get('from_date')))
            {
                $saleOff->from_date=$this->request->get('from_date');
            }
             if(!empty($this->request->get('code')))
            {
                $saleOff->code=$this->request->get('code');
            }
             if(!empty($this->request->get('to_date')))
            {
                $saleOff->to_date=$this->request->get('to_date');
            }

         
            
            $saleOff->save();
        }
        return $this->successRequest($saleOff); 
    }
    
    public function delete()
    {

        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $saleOff=SaleOff::where('_id',mongo_id($id))->first();
        if(!empty($saleOff))
        {   
            $saleOff->forceDelete();
        }
        return $this->successRequest();
    }
    public function list()
    {

       $saleOff = SaleOff::all();
       if(count($saleOff)>0)
       {
         foreach ($saleOff as $key => $value) {
             $saleOff_list[]= $value;
                }
                return $saleOff_list;
       }
       return [];
    }
    public function find()
    {
        $SaleOff=[];

        $validator = \validator::make($this->request->all(),[
            'id'=>'required', 
           
        ]);
        $id = $this->request->get('_id');
       $SaleOff = SaleOff::where('_id',mongo_id($id))->first();
       
       return $SaleOff;
    }
       public function check_code()
    {
        $SaleOff=[];
         // $firstDay = Carbon::create(
         //                $this->request->get('year'),
         //                $this->request->get('month'),
         //                1,
         //                0,
         //                0,
         //                0
         //            );
        $validator = \validator::make($this->request->all(),[
            'code'=>'required', 
           
        ]);
        $code = $this->request->get('code');
       $saleOff = SaleOff::where('code',$code)->first();

       if($saleOff)
       {
          $today = Carbon::today();
           $from_date = Carbon::parse($saleOff->from_date)->format('m/d/Y');
            $to_date = Carbon::parse($saleOff->to_date)->format('m/d/Y');
            if( strtotime($today)>= strtotime($from_date) && strtotime($today)<= strtotime($to_date))
            {
                return $saleOff;
            }
            else{
                return 0;
            }

       }
       else{
        return 0;
       }
       
     
    }
}