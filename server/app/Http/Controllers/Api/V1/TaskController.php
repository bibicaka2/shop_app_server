<?php
namespace App\Http\Controllers\Api\V1;

// use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\TaskRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Task;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class TaskController extends Controller
{
    //protected $userRepository;
    protected $taskRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        // UserRepository $userRepository,
        TaskRepository $taskRepository,
        AuthManager $auth,
        Request $request
    )   {
           // $this->userRepository = $userRepository;
            $this->taskRepository = $taskRepository;
            $this->request = $request;
            $this->auth = $auth;
            parent::__construct();
    }
    public function create()
    {
        $validator = \validator::make($this->request->all(),[
            'todoid'=>'required', 
            'name'=>'required', 
            'isfinished'=>'required', 
           
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
       
        $attributes = [  
            'todoid' => $this->request->get('todoid'),  
            'name' => $this->request->get('name'),  
            'isfinished' => $this->request->get('isfinished'),  
           
          
        ];

        $task =$this->taskRepository->create($attributes);
       // return $this->successRequest($branch->transform('for-detail'));
        return $this->successRequest($task);

    }
     public function update() {
        // Input
         $validator = \validator::make($this->request->all(),[
            'id'=>'required',
            'todoid'=>'required', 
            'name'=>'required', 
            'isfinished'=>'required', 
        ]);
        $id = $this->request->get('_id');
        $task = Task::where('_id', mongo_id($id))->first();
        if (!empty($task))
        { 
            if(!empty($this->request->get('todoid')))
                {
                    $task->todoid=$this->request->get('todoid');
                }
                 if(!empty($this->request->get('name')))
                {
                    $task->name=$this->request->get('name');
                }
                 if(!empty($this->request->get('isfinished')))
                {
                    $task->isfinished=$this->request->get('isfinished');
                }
            
            $task->save();
        }
         return $this->successRequest($task);
    }
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
        ]);
        $id=$this->request->get('_id');
        $task=Task::where('_id',mongo_id($id))->first();
        if(!empty($task))
        {
            $task->delete();
        }
        return $this->successRequest();
    }
    public function view()
    {
        $task = Task::get();
        $viewData = [
            'Task' => $task
        ];
        return $viewData;
    }
    public function find(){
         $todoid= $this->request->get('todoid');
        $task[] = Task::where('todoid',$todoid)->first();
        $view_Data=[
            'Task' => $task,
            ];
        return $view_Data;

    }


}