
<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\TestRepository;
use App\Api\Repositories\Contracts\loginRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Test;
use App\Api\Entities\Login;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


use Redirect,Response,DB,Config;
use Illuminate\Support\Facades\Mail;

class TestController extends Controller
{
    protected $userRepository;
    protected $testRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        UserRepository $userRepository,
        TestRepository $testRepository,
        AuthManager $auth,
        Request $request
    )   {   
            $this->userRepository = $userRepository;
            $this->testRepository = $testRepository;
            $this->request = $request;
            $this->auth = $auth;
            parent::__construct();
    }
    public function create()
    {
         $validator = \validator::make($this->request->all(),[
            'username'=>'required',
            'password'=>'required',
            'name'=>'required',

        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $login= Login::where('username',$this->request->get('username'))->where('password',$this->request->get('password'))->first();
        if(!empty($login))
        {

            $attributes = [  
                'name' => $this->request->get('name') 
            ];
             $test =$this->testRepository->create($attributes);
               return $this->successRequest($test);
                //return $this->successRequest([trans('success')]);
        }
        else
        {
            dd('vui long nhap lai');
        }
    }
     public function update() {
        // Input
         $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
            'name'=>'required',
        ]);
        $id = $this->request->get('_id');
        $test = Test::where('_id', mongo_id($id))->first();
        if (!empty($test))
        { 
            if(!empty($this->request->get('name')))
                {
                    $test->name=$this->request->get('name');
                }
            $test->save();
        }
         return $this->successRequest($test);
    }
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',
        ]);
        $id=$this->request->get('_id');
        $test=Test::where('_id',mongo_id($id))->first();
        if(!empty($test))
        {
            $test->delete();
        }
        return $this->successRequest();
    }
    public function view()
    {
        $test = Test::get();
        $viewData = [
            'Test' => $test
        ];
        return $viewData;
    }
    public function go()
    {
    //    $date=date('H:i:s');
        $date=Carbon::Parse();

     //   $date1=date('H',$date);
        dd($date);
    }
    public function test()
    {
         $validator = \validator::make($this->request->all(),[
            'username'=>'required',
            'password'=>'required',
            'name'=>'required',

        ]);

           
            $attributes = [  
                'name' => $this->request->get('name'),
                'test'=>[
                    'username'=>$this->request->get('username'),
                    'password'=>$this->request->get('password'),
                ]
            ];
             $test =$this->testRepository->create($attributes);
               return $this->successRequest($test);
          //return $this->successRequest([trans('success')]);
         
    }
    
}