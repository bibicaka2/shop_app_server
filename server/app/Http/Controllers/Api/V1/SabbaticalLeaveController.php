<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\SabbaticalLeaveRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\SabbaticalLeave;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;

class SabbaticalLeaveController extends Controller
{
    protected $userRepository;
    protected $sabbaticalleaveRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        UserRepository $userRepository,
        SabbaticalLeaveRepository $sabbaticalleaveRepository,
        AuthManager $auth,
        Request $request
    ) {
        $this->userRepository = $userRepository;
        $this->sabbaticalleaveRepository = $sabbaticalleaveRepository;
        $this->request = $request;
        $this->auth = $auth;
        parent::__construct();
    }

    public function create()
    {
       
        $validator = \validator::make($this->request->all(),[
            
            'id_shift_work'=>'required',
            'date' =>'required',
            'month'=>'required',
            
        ]);
        
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
       // $id_shift_work=$this->request->get('id_shift_work');
      //  $day= Carbon::Parse($this->request->get('day'));
        
        $attributes = [                 
            'id_shift_work'=>$this->request->get('id_shift_work'),
            'date'=>$this->request->get('date'),
            'month'=>$this->request->get('month'),
            'time_from'=>(int)$this->request->get('time_from'),
            'time_to'=>(int)$this->request->get('time_to'),

        ];  
        
        $sabbaticalleave =$this->sabbaticalleaveRepository->create($attributes);
        
        return $this->successRequest($sabbaticalleave);
    }
    
    public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            'id_shift_work'=>'required',
            'date'=>'required|INT', 
            'month'=>'required',
            'time_from'=>'required',
            'time_to'=>'required',


        ]);

        $id = $this->request->get('_id');
        $sabbaticalleave = SabbaticalLeave::where([
            '_id'=> mongo_id($id),
        ])->first();
        
        if (!empty($sabbaticalleave)) {
           //  if(!empty($this->request->get('timein'))){$nhanvien1->name=$this->request->get('timein');}
            if(!empty($this->request->get('id_shift_work')))
            {
                $sabbaticalleave->id_shift_work=$this->request->get('id_shift_work');
            }
            if(!empty($this->request->get('date')))
            {
                $sabbaticalleave->date=$this->request->get('date');
            }
            if(!empty($this->request->get('month')))
            {
                $sabbaticalleave->day=$this->request->get('month');
            }
              if(!empty($this->request->get('time_from')))
            {
                $sabbaticalleave->time_from=(int)$this->request->get('time_from');
            }
            if(!empty($this->request->get('time_to')))
            {
                $sabbaticalleave->time_to=(int)$this->request->get('time_to');
            }
            $sabbaticalleave->save();
        }
        
        return $this->successRequest($sabbaticalleave);
    }
    
    public function delete()
    {
         $validator = \validator::make($this->request->all(),[
            '_id' =>'required',
        ]);
        $id=$this->request->get('_id');     
        $sabbaticalleave=ShiftWork::where('_id',mongo_id($id))->first();
        if(!empty($sabbaticalleave))
        {
            $sabbaticalleave->delete();
        }
        return $this->successRequest(trans('core.success'));
    }
    public function view()
    {
      // $date="1999/07/17"; 
        //$time = strtotime('2020/07/17');
       // $time1 = strtotime($date);

        // $newformat1 = date('Y:m:d',$time1);
     //    $newformat = date('Y-m-d 00:00:00');
     // //   $time1 = strtotime($newformat);
     //  //  dd($newformat);
     //    $sabbaticalleave= SabbaticalLeave::where('day',$newformat)->first();
     //    dd($sabbaticalleave);
     // $date= Carbon::Parse($this->request->get('date'));
     // dd($date->format);
     // $date1 = date('Y:m:d');
     //  dd($date1);
     


     // $test2=date("Y",strtotime($test));
       // dd($date1);
    }

   
}