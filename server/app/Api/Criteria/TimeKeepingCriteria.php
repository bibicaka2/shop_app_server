<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class BranchCriteria
 */
class TimeKeepingCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();

        if(!empty($this->params['_id'])) {
            $query->where('_id',$this->params['_id']);
        }
        if(!empty($this->params['id_shift_work'])) {
             $query->where('id_shift_work',$this->params['id_shift_work']);
        }
        if(!empty($this->params['status'])) {
             $query->where('status',$this->params['status']);
        }



        // if(!empty($this->params['seller_id'])) {
        //     $query->where('seller_id',$this->params['seller_id']);
        // }
        // }


        
        $query->orderBy('sort_index', 'asc');
        $query->orderBy('updated_at', 'asc');
        //Set language
        // $query->where('lang',app('translator')->getLocale());
        return $query;
    }
}
