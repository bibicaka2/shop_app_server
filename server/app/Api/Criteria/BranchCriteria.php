<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class BranchCriteria
 */
class BranchCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();

        if(!empty($this->params['branch_address'])) {
            $query->where('address',$this->params['branch_address']);
        }
        if(!empty($this->params['_id'])) {
            $query->where('_id',mongo_id($this->params['_id']));
        }
        if(!empty($this->params['keyword'])) { 
            $query->where('address','like',"%".$this->params['keyword']."%");
        }
        if(!empty($this->params['key'])) { 
            $query->where('address','like',"%".$this->params['key']."%");
        }
              
       // $query->orderBy('sort_index', 'asc');
        $query->orderBy('updated_at', 'asc');
        //Set language
        // $query->where('lang',app('translator')->getLocale());
        return $query;
    }
}
