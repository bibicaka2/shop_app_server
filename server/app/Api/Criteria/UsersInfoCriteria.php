<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;
/**
 * Class UsersInfoCriteria
 */
class UsersInfoCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();
        
        if(!empty($this->params['_id'])){
            $query->where('_id',mongo_id($this->params['_id']));
        }

        if(!empty($this->params['nation'])){
            $nation = '%'.(string)($this->params['nation']).'%';
            $query->where('nation','like',$nation);
        }
        
        if(!empty($this->params['email'])){
            $email = '%'.(string)($this->params['email']).'%';
            $query->where('email','like',$email);
        }
        
        if(!empty($this->params['phone'])){//dd('sdg');
            $phone = '%'.(string)($this->params['phone']).'%';
            $query->where('phone','like',$phone);
        }
        return $query;
    }
}
