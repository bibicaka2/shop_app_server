<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\TodosTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Todos extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'Todos';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new TodosTransformer();

        return $transformer->transform($this);
    }

}
