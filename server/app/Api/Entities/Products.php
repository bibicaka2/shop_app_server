<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\ProductsTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Products extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'Products';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new ProductsTransformer();

        return $transformer->transform($this);
    }

}
