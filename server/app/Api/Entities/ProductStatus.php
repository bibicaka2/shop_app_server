<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\ProductStatusTransformer;
use Moloquent\Eloquent\SoftDeletes;

class ProductStatus extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'product_status';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new ProductStatusTransformer();

        return $transformer->transform($this);
    }

}
