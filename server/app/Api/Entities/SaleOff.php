<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\SaleOffTransformer;
use Moloquent\Eloquent\SoftDeletes;

class SaleOff extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'sale_off';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new SaleOffTransformer();

        return $transformer->transform($this);
    }

}
