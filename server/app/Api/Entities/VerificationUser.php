<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\VerificationUserTransformer;
use Moloquent\Eloquent\SoftDeletes;

class VerificationUser extends Moloquent
{
	use SoftDeletes;

    protected $collection = 'verification_user';

    protected $guarded = array();


    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new VerificationUserTransformer();

        return $transformer->transform($this);
    }

}
