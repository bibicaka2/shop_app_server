<?php
namespace App\Api\Entities;
use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\LoginTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Login extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'Login';

    protected $guarded = array();
    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform($type='')
    {
        $transformer = new LoginTransformer();
       
        return $transformer->transform($this,$type);
    }

}
