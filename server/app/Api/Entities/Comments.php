<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\CommentsTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Comments extends Moloquent
{
	use SoftDeletes;

    protected $collection = 'Comments';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new CommentsTransformer();

        return $transformer->transform($this);
    }

}
