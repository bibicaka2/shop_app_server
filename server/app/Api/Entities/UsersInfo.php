<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UsersInfoTransformer;
use Moloquent\Eloquent\SoftDeletes;

class UsersInfo extends Moloquent
{
    use SoftDeletes;

   protected $collection = 'users_info';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $type = '')
    {
        $transformer = new UsersInfoTransformer();

        return $transformer->transform($this,$type);
    }

}
