<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UsersTransformer;
use Moloquent\Eloquent\SoftDeletes;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject; 

class Users extends Moloquent implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use SoftDeletes;
    use Authenticatable,Authorizable;

    protected $collection = 'users';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at','password'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }

    public function transform()
    {
        $transformer = new UsersTransformer();

        return $transformer->transform($this);
    }

}
