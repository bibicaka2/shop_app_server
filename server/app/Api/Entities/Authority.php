<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\AuthorityTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Authority extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'Authority';

     protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new AuthorityTransformer();

        return $transformer->transform($this);
    }

}
