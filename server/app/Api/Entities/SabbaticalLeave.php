<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\SabbaticalLeaveTransformer;
use Moloquent\Eloquent\SoftDeletes;

class SabbaticalLeave extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'SabbaticalLeave';
    protected $guarded = array();
    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'day',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new SabbaticalLeaveTransformer();

        return $transformer->transform($this);
    }

}
