<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\ProductTypeTransformer;
use Moloquent\Eloquent\SoftDeletes;

class ProductType extends Moloquent
{
	use SoftDeletes;
protected $collection = 'product_type';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new ProductTypeTransformer();

        return $transformer->transform($this);
    }

}
        