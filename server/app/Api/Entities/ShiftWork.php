<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\ShiftWorkTransformer;
use Moloquent\Eloquent\SoftDeletes;

class ShiftWork extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'ShiftWork';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new ShiftWorkTransformer();

        return $transformer->transform($this);
    }

}
