<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UserTokenTransformer;
use Moloquent\Eloquent\SoftDeletes;

class UserToken extends Moloquent
{
    use SoftDeletes;

    protected $collection = 'user_tokens';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new UserTokenTransformer();

        return $transformer->transform($this);
    }

}
