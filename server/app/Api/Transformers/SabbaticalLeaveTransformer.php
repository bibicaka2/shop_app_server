<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\SabbaticalLeave;

/**
 * Class SabbaticalLeaveTransformer
 */
class SabbaticalLeaveTransformer extends TransformerAbstract
{

    /**
     * Transform the \SabbaticalLeave entity
     * @param \SabbaticalLeave $model
     *
     * @return array
     */
    public function transform(SabbaticalLeave $model)
    {
        return [
            'id'         => $model->_id,

            
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
