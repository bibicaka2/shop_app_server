<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Staff;

/**
 * Class StaffTransformer
 */
class StaffTransformer extends TransformerAbstract
{

    /**
     * Transform the \Staff entity
     * @param \Staff $model
     *
     * @return array
     */
    public function transform(Staff $model,$type='')
    {

        if($type=='info')
        {
            $data=[
                '_id'=>$model->_id,
                'name'=>$model->name,
            ];
            return $data;
        }
    }
}
