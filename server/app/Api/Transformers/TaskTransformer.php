<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Task;

/**
 * Class TaskTransformer
 */
class TaskTransformer extends TransformerAbstract
{

    /**
     * Transform the \Task entity
     * @param \Task $model
     *
     * @return array
     */
    public function transform(Task $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
