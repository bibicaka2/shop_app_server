<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Login;

/**
 * Class LoginTransformer
 */
class LoginTransformer extends TransformerAbstract
{

    /**
     * Transform the \Login entity
     * @param \Login $model
     *
     * @return array
     */
    public function transform(Login $model,$type='')
    {
        $data=[
            'displayname'=>$model->displayname,
            'username'=>$model->username,
            'password'=>$model->password
        ];

        $data_comments=[    
            'username'=>$model->username,
            'displayname'=>$model->displayname,
        ];

        if($type==='user_list')
        {
        
            return $data;
        }
        if($type==='user_view')
        {
            return $data;
        }
        if($type=== 'user_comment')
        {      
            return $data_comments;
        }
        // return [
        //     'id'         => $model->_id,

            

        //     'created_at' => $model->created_at,
        //     'updated_at' => $model->updated_at
        // ];
    }
}
