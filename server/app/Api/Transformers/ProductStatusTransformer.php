<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\ProductStatus;

/**
 * Class ProductStatusTransformer
 */
class ProductStatusTransformer extends TransformerAbstract
{

    /**
     * Transform the \ProductStatus entity
     * @param \ProductStatus $model
     *
     * @return array
     */
    public function transform(ProductStatus $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
