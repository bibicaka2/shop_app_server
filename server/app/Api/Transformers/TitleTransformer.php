<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Title;

/**
 * Class TitleTransformer
 */
class TitleTransformer extends TransformerAbstract
{

    /**
     * Transform the \Title entity
     * @param \Title $model
     *
     * @return array
     */
    public function transform(Title $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
