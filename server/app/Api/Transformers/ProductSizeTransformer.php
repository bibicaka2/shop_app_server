<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\ProductSize;

/**
 * Class ProductSizeTransformer
 */
class ProductSizeTransformer extends TransformerAbstract
{

    /**
     * Transform the \ProductSize entity
     * @param \ProductSize $model
     *
     * @return array
     */
    public function transform(ProductSize $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
