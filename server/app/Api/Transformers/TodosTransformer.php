<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Todos;

/**
 * Class TodosTransformer
 */
class TodosTransformer extends TransformerAbstract
{

    /**
     * Transform the \Todos entity
     * @param \Todos $model
     *
     * @return array
     */
    public function transform(Todos $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
