<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\ChangLan;

/**
 * Class ChangLanTransformer
 */
class ChangLanTransformer extends TransformerAbstract
{

    /**
     * Transform the \ChangLan entity
     * @param \ChangLan $model
     *
     * @return array
     */
    public function transform(ChangLan $model)
    {
        return [
            'id'         => $model->_id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
