<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\SaleOff;

/**
 * Class SaleOffTransformer
 */
class SaleOffTransformer extends TransformerAbstract
{

    /**
     * Transform the \SaleOff entity
     * @param \SaleOff $model
     *
     * @return array
     */
    public function transform(SaleOff $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
