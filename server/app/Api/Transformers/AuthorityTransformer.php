<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Authority;

/**
 * Class AuthorityTransformer
 */
class AuthorityTransformer extends TransformerAbstract
{

    /**
     * Transform the \Authority entity
     * @param \Authority $model
     *
     * @return array
     */
    public function transform(Authority $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
