<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Libraries\Gma\APIs\APIUpload;
use DateTime;

/**
 * Class UsersTransformer
 */
class UsersTransformer extends TransformerAbstract
{

    /**
     * Transform the \Users entity
     * @param \Users $model
     *
     * @return array
     */
    public function transform(Users $model)
    {
        $data = [
            'user_id' => $model->user_id,
            'user_name' => $model->user_name,
            'no_sign' => $model->no_sign,
            'no_sign_profile' => $model->no_sign_profile
        ];

        $userInfo = UsersInfo::where([
            '_id' => mongo_id($model->user_id)
        ])->first();

        if (!empty($userInfo)) {
            $data['first_name'] = $userInfo->first_name;
            $data['last_name'] = $userInfo->last_name;
            $data['phone'] = $userInfo->phone;
            $data['email'] = $userInfo->email;


            $createDateTime = DateTime::createFromFormat("d/m/Y", $userInfo->dOb);
            $data['dOb'] = $createDateTime->format('d/m/Y');

            if ($userInfo->sex == 0) {
                $data['sex'] = false;
            } else {
                $data['sex'] = true;
            }
        }

        //get user avatar
        $params = [
            'type' => 'image',
            'user_id' => $data['user_id'],
            'option' => 'avatars'
        ];
        $avatarURI = APIUpload::getFileToClient($params);
        $data['user_avatar'] = $avatarURI;

        //get user cover
        $params = [
            'type' => 'image',
            'user_id' => $data['user_id'],
            'option' => 'cover_picture'
        ];
        $coverURI = APIUpload::getFileToClient($params);
        $data['user_cover'] = $coverURI;

        return $data;
    }
}
