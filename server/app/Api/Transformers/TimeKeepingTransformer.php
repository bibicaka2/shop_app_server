<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\TimeKeeping;

/**
 * Class TimeKeepingTransformer
 */
class TimeKeepingTransformer extends TransformerAbstract
{

    /**
     * Transform the \TimeKeeping entity
     * @param \TimeKeeping $model
     *
     * @return array
     */
    public function transform(TimeKeeping $model,$type='',$data1)
    {
        // return [
        //     'id'         => $model->_id,
        //     'created_at' => $model->created_at,
        //     'updated_at' => $model->updated_at
        // ];

       $data=[
            'id'         => $model->_id,
            'id_shift_work'=>$model->id_shift_work,
            'timein' => $model->timein,
            'timeout' => $model->timeout,  
            'info'=>$data1,    
        ];
       if($type == 'TimeKeeping_list'){
            //'id'=>$model->_id;
            return [
                $data,
            ];
        }
        return [];
    }
}
