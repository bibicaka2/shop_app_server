<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Department;

/**
 * Class DepartmentTransformer
 */
class DepartmentTransformer extends TransformerAbstract
{

    /**
     * Transform the \Department entity
     * @param \Department $model
     *
     * @return array
     */
    public function transform(Department $model)
    {
        return [
            'id'         => $model->_id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
