<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\todosRepository;
use App\Api\Entities\Todos;
use App\Api\Validators\TodosValidator;

/**
 * Class TodosRepositoryEloquent
 */
class TodosRepositoryEloquent extends BaseRepository implements TodosRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Todos::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
