<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\UserTokenRepository;
use App\Api\Entities\UserToken;
use App\Api\Validators\UserTokenValidator;

/**
 * Class UserTokenRepositoryEloquent
 */
class UserTokenRepositoryEloquent extends BaseRepository implements UserTokenRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserToken::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
