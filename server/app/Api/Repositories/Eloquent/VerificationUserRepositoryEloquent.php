<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\verificationUserRepository;
use App\Api\Entities\VerificationUser;
use App\Api\Validators\VerificationUserValidator;

/**
 * Class VerificationUserRepositoryEloquent
 */
class VerificationUserRepositoryEloquent extends BaseRepository implements VerificationUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return VerificationUser::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
