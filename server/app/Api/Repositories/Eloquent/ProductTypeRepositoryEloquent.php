<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\ProductTypeRepository;
use App\Api\Entities\ProductType;
use App\Api\Validators\ProductTypeValidator;

/**
 * Class ProductTypeRepositoryEloquent
 */
class ProductTypeRepositoryEloquent extends BaseRepository implements ProductTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductType::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
