<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\StaffRepository;
use App\Api\Entities\Staff;
use App\Api\Validators\StaffValidator;

/**
 * Class StaffRepositoryEloquent
 */
class StaffRepositoryEloquent extends BaseRepository implements StaffRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Staff::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
