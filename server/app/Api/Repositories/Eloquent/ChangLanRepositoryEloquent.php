<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\ChangLanRepository;
use App\Api\Entities\ChangLan;
use App\Api\Validators\ChangLanValidator;

/**
 * Class ChangLanRepositoryEloquent
 */
class ChangLanRepositoryEloquent extends BaseRepository implements ChangLanRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ChangLan::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
