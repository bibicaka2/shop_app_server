<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductTypeRepository
 */
interface ProductTypeRepository extends RepositoryInterface
{
    
}
