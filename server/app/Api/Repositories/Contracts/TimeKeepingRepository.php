<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\TimeKeeping;
/**
 * Interface TimeKeepingRepository
 */
interface TimeKeepingRepository extends RepositoryInterface
{
      public function getTimeKeeping($params = [],$limit = 0);
}
