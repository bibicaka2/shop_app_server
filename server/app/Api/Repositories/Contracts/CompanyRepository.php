<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\Company;
/**
 * Interface CompanyRepository
 */
interface CompanyRepository extends RepositoryInterface
{
    
}
