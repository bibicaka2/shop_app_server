<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductSizeRepository
 */
interface ProductSizeRepository extends RepositoryInterface
{
    
}
