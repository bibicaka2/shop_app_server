<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserInfoRepository
 */
interface UsersInfoRepository extends RepositoryInterface
{
      public function getUserInfo($params = [], $limit = 0);
}
