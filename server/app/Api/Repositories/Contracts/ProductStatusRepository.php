<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductStatusRepository
 */
interface ProductStatusRepository extends RepositoryInterface
{
    
}
