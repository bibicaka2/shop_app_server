<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VerificationUserRepository
 */
interface VerificationUserRepository extends RepositoryInterface
{
    
}
