<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TaskRepository
 */
interface TaskRepository extends RepositoryInterface
{
    
}
