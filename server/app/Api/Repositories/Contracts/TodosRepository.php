<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TodosRepository
 */
interface TodosRepository extends RepositoryInterface
{
    
}
