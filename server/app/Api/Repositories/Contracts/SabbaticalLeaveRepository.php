<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\SabbaticalLeave;
/**
 * Interface SabbaticalLeaveRepository
 */
interface SabbaticalLeaveRepository extends RepositoryInterface
{
    
}
