<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\Staff;
/**
 * Interface StaffRepository
 */
interface StaffRepository extends RepositoryInterface
{
    
}
