<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LoginRepository
 */
interface LoginRepository extends RepositoryInterface
{
    
}
