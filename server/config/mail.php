<?php
return [
    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host' => env('MAIL_HOST', 'smtp.gmail.com'),
    'port' => env('MAIL_PORT', 587),
    'from' => ['address' => 'tranhungtien1199@gmail.com', 'name' => 'Admin Shop'],
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    'username' => env('MAIL_USERNAME','tranhungtien1199'),
    'password' => env('MAIL_PASSWORD','hungphuoc1'),
    'sendmail' => '/usr/sbin/sendmail -bs',
    'pretend' => false,
];